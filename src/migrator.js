import "dotenv/config";
import { db } from "./models";
import seeds from "./seeds";

async function migrate() {
  db.transaction(async (dbTrx) => {
    // display the log
    db.options.logging = console.log;
    // sync to db
    await db.sync({ alter: true });
    console.log("Database migration finished");
    await db.query(seeds);
    console.log("Data seed generated");
  });
}

export default migrate();
