let seeds = "";

// school period
seeds += `
INSERT INTO school_periods (id, start_year, end_year, semester, created_at, updated_at) VALUES (1, 2023, 2024, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE id = VALUES (id);
`;


// role
seeds += `
INSERT INTO roles (code, name, is_default) VALUES ('superadmin', 'Superadmin', true) ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO roles (code, name) VALUES ('teacher', 'Guru') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO roles (code, name) VALUES ('student', 'Peserta Didik') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO roles (code, name) VALUES ('principal', 'Kepala Sekolah') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO roles (code, name) VALUES ('homeroom_teacher', 'Wali Kelas') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO roles (code, name) VALUES ('finance', 'Bendahara') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO roles (code, name) VALUES ('attendance_system', 'Sistem Kehadiran') ON DUPLICATE KEY UPDATE code = VALUES (code);
`;

// user
seeds += `
INSERT INTO users (id, username, password, name, is_reset, is_active, created_at, updated_at) VALUES (1, 'superadmin', '$2a$12$Lhi/RdXVMuSoHjzqZHJ4pOH5F37idyyFIfnShHzYRDvGyfe0wW0FS', 'Saais Superadmin', false, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO users (id, username, password, name, is_reset, is_active, created_at, updated_at) VALUES (2, 'attendance_system', '$2a$12$Lhi/RdXVMuSoHjzqZHJ4pOH5F37idyyFIfnShHzYRDvGyfe0wW0FS', 'Sistem Kehadiran', false, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO users (id, username, password, name, is_reset, is_active, created_at, updated_at) VALUES (3, 'student', '$2a$12$Lhi/RdXVMuSoHjzqZHJ4pOH5F37idyyFIfnShHzYRDvGyfe0wW0FS', 'Siswa', false, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO users (id, username, password, name, is_reset, is_active, created_at, updated_at) VALUES (4, 'homeroom_teacher', '$2a$12$Lhi/RdXVMuSoHjzqZHJ4pOH5F37idyyFIfnShHzYRDvGyfe0wW0FS', 'Wali Kelas', false, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO users (id, username, password, name, is_reset, is_active, created_at, updated_at) VALUES (5, 'teacher', '$2a$12$Lhi/RdXVMuSoHjzqZHJ4pOH5F37idyyFIfnShHzYRDvGyfe0wW0FS', 'Guru', false, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO users (id, username, password, name, is_reset, is_active, created_at, updated_at) VALUES (6, 'homeroom_teacher2', '$2a$12$Lhi/RdXVMuSoHjzqZHJ4pOH5F37idyyFIfnShHzYRDvGyfe0wW0FS', 'Wali Kelas 2', false, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO users (id, username, password, name, is_reset, is_active, created_at, updated_at) VALUES (7, 'teacher2', '$2a$12$Lhi/RdXVMuSoHjzqZHJ4pOH5F37idyyFIfnShHzYRDvGyfe0wW0FS', 'Guru 2', false, true, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE id = VALUES (id);
`;

// user role
seeds += `
INSERT INTO user_roles (user_id, code) VALUES (1, 'superadmin') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO user_roles (user_id, code) VALUES (2, 'attendance_system') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO user_roles (user_id, code) VALUES (3, 'student') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO user_roles (user_id, code) VALUES (4, 'homeroom_teacher') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO user_roles (user_id, code) VALUES (5, 'teacher') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO user_roles (user_id, code) VALUES (6, 'homeroom_teacher') ON DUPLICATE KEY UPDATE code = VALUES (code);
INSERT INTO user_roles (user_id, code) VALUES (7, 'teacher') ON DUPLICATE KEY UPDATE code = VALUES (code);
`;

// class major
seeds += `
INSERT INTO class_majors (id, name) VALUES ('1', 'Umum') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO class_majors (id, name) VALUES ('2', 'IPA') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO class_majors (id, name) VALUES ('3', 'IPS') ON DUPLICATE KEY UPDATE id = VALUES (id);
`;

// // city
seeds += `
INSERT INTO cities (id,name,province_name) VALUES
	 (1,'Badung','Bali'),
	 (2,'Bangli','Bali'),
	 (3,'Buleleng','Bali'),
	 (4,'Denpasar','Bali'),
	 (5,'Gianyar','Bali'),
	 (6,'Jembrana','Bali'),
	 (7,'Karangasem','Bali'),
	 (8,'Klungkung','Bali'),
	 (9,'Tabanan','Bali'),
	 (10,'Bima','Nusa Tenggara Barat (NTB)') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (11,'Dompu','Nusa Tenggara Barat (NTB)'),
	 (12,'Lombok Barat','Nusa Tenggara Barat (NTB)'),
	 (13,'Lombok Tengah','Nusa Tenggara Barat (NTB)'),
	 (14,'Lombok Timur','Nusa Tenggara Barat (NTB)'),
	 (15,'Lombok Utara','Nusa Tenggara Barat (NTB)'),
	 (16,'Mataram','Nusa Tenggara Barat (NTB)'),
	 (17,'Sumbawa','Nusa Tenggara Barat (NTB)'),
	 (18,'Sumbawa Barat','Nusa Tenggara Barat (NTB)'),
	 (19,'Balikpapan','Kalimantan Timur'),
	 (20,'Berau','Kalimantan Timur') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (21,'Bontang','Kalimantan Timur'),
	 (22,'Kutai Barat','Kalimantan Timur'),
	 (23,'Kutai Kartanegara','Kalimantan Timur'),
	 (24,'Kutai Timur','Kalimantan Timur'),
	 (25,'Paser','Kalimantan Timur'),
	 (26,'Penajam Paser Utara','Kalimantan Timur'),
	 (27,'Samarinda','Kalimantan Timur'),
	 (28,'Batang Hari','Jambi'),
	 (29,'Bungo','Jambi'),
	 (30,'Jambi','Jambi') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (31,'Kerinci','Jambi'),
	 (32,'Merangin','Jambi'),
	 (33,'Muaro Jambi','Jambi'),
	 (34,'Sarolangun','Jambi'),
	 (35,'Sungaipenuh','Jambi'),
	 (36,'Tanjung Jabung Barat','Jambi'),
	 (37,'Tanjung Jabung Timur','Jambi'),
	 (38,'Tebo','Jambi'),
	 (39,'Bengkalis','Riau'),
	 (40,'Dumai','Riau') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (41,'Indragiri Hilir','Riau'),
	 (42,'Indragiri Hulu','Riau'),
	 (43,'Kampar','Riau'),
	 (44,'Kepulauan Meranti','Riau'),
	 (45,'Kuantan Singingi','Riau'),
	 (46,'Pekanbaru','Riau'),
	 (47,'Pelalawan','Riau'),
	 (48,'Rokan Hilir','Riau'),
	 (49,'Rokan Hulu','Riau'),
	 (50,'Siak','Riau') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (51,'Pangkajene Kepulauan','Sulawesi Selatan'),
	 (52,'Sidenreng Rappang/Rapang','Sulawesi Selatan'),
	 (53,'Wajo','Sulawesi Selatan'),
	 (54,'Bulukumba','Sulawesi Selatan'),
	 (55,'Palopo','Sulawesi Selatan'),
	 (56,'Sinjai','Sulawesi Selatan'),
	 (57,'Jeneponto','Sulawesi Selatan'),
	 (58,'Bantaeng','Sulawesi Selatan'),
	 (59,'Luwu','Sulawesi Selatan'),
	 (60,'Luwu Utara','Sulawesi Selatan') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (61,'Selayar (Kepulauan Selayar)','Sulawesi Selatan'),
	 (62,'Tana Toraja','Sulawesi Selatan'),
	 (63,'Soppeng','Sulawesi Selatan'),
	 (64,'Barru','Sulawesi Selatan'),
	 (65,'Luwu Timur','Sulawesi Selatan'),
	 (66,'Parepare','Sulawesi Selatan'),
	 (67,'Enrekang','Sulawesi Selatan'),
	 (68,'Makassar','Sulawesi Selatan'),
	 (69,'Toraja Utara','Sulawesi Selatan'),
	 (70,'Takalar','Sulawesi Selatan') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (71,'Pinrang','Sulawesi Selatan'),
	 (72,'Gowa','Sulawesi Selatan'),
	 (73,'Bone','Sulawesi Selatan'),
	 (74,'Maros','Sulawesi Selatan'),
	 (75,'Surabaya','Jawa Timur'),
	 (76,'Pasuruan','Jawa Timur'),
	 (77,'Blitar','Jawa Timur'),
	 (78,'Ponorogo','Jawa Timur'),
	 (79,'Bondowoso','Jawa Timur'),
	 (80,'Situbondo','Jawa Timur') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (81,'Sumenep','Jawa Timur'),
	 (82,'Nganjuk','Jawa Timur'),
	 (83,'Jombang','Jawa Timur'),
	 (84,'Bangkalan','Jawa Timur'),
	 (85,'Lamongan','Jawa Timur'),
	 (86,'Bojonegoro','Jawa Timur'),
	 (87,'Madiun','Jawa Timur'),
	 (88,'Gresik','Jawa Timur'),
	 (89,'Pacitan','Jawa Timur'),
	 (90,'Tulungagung','Jawa Timur') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (91,'Ngawi','Jawa Timur'),
	 (92,'Magetan','Jawa Timur'),
	 (93,'Lumajang','Jawa Timur'),
	 (94,'Banyuwangi','Jawa Timur'),
	 (95,'Mojokerto','Jawa Timur'),
	 (96,'Malang','Jawa Timur'),
	 (97,'Trenggalek','Jawa Timur'),
	 (98,'Batu','Jawa Timur'),
	 (99,'Sampang','Jawa Timur'),
	 (100,'Tuban','Jawa Timur') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (101,'Probolinggo','Jawa Timur'),
	 (102,'Kediri','Jawa Timur'),
	 (103,'Pamekasan','Jawa Timur'),
	 (104,'Sidoarjo','Jawa Timur'),
	 (105,'Jember','Jawa Timur'),
	 (106,'Samosir','Sumatera Utara'),
	 (107,'Padang Lawas','Sumatera Utara'),
	 (108,'Padang Lawas Utara','Sumatera Utara'),
	 (109,'Tanjung Balai','Sumatera Utara'),
	 (110,'Padang Sidempuan','Sumatera Utara') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (111,'Binjai','Sumatera Utara'),
	 (112,'Tapanuli Tengah','Sumatera Utara'),
	 (113,'Asahan','Sumatera Utara'),
	 (114,'Medan','Sumatera Utara'),
	 (115,'Batu Bara','Sumatera Utara'),
	 (116,'Labuhan Batu','Sumatera Utara'),
	 (117,'Tebing Tinggi','Sumatera Utara'),
	 (118,'Toba Samosir','Sumatera Utara'),
	 (119,'Tapanuli Selatan','Sumatera Utara'),
	 (120,'Deli Serdang','Sumatera Utara') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (121,'Nias Utara','Sumatera Utara'),
	 (122,'Labuhan Batu Utara','Sumatera Utara'),
	 (123,'Sibolga','Sumatera Utara'),
	 (124,'Labuhan Batu Selatan','Sumatera Utara'),
	 (125,'Langkat','Sumatera Utara'),
	 (126,'Mandailing Natal','Sumatera Utara'),
	 (127,'Nias Barat','Sumatera Utara'),
	 (128,'Serdang Bedagai','Sumatera Utara'),
	 (129,'Karo','Sumatera Utara'),
	 (130,'Nias Selatan','Sumatera Utara') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (131,'Dairi','Sumatera Utara'),
	 (132,'Gunungsitoli','Sumatera Utara'),
	 (133,'Humbang Hasundutan','Sumatera Utara'),
	 (134,'Simalungun','Sumatera Utara'),
	 (135,'Pakpak Bharat','Sumatera Utara'),
	 (136,'Pematang Siantar','Sumatera Utara'),
	 (137,'Tapanuli Utara','Sumatera Utara'),
	 (138,'Nias','Sumatera Utara'),
	 (139,'Musi Rawas','Sumatera Selatan'),
	 (140,'Banyuasin','Sumatera Selatan') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (141,'Pagar Alam','Sumatera Selatan'),
	 (142,'Lahat','Sumatera Selatan'),
	 (143,'Ogan Ilir','Sumatera Selatan'),
	 (144,'Empat Lawang','Sumatera Selatan'),
	 (145,'Ogan Komering Ilir','Sumatera Selatan'),
	 (146,'Lubuk Linggau','Sumatera Selatan'),
	 (147,'Palembang','Sumatera Selatan'),
	 (148,'Ogan Komering Ulu Timur','Sumatera Selatan'),
	 (149,'Musi Banyuasin','Sumatera Selatan'),
	 (150,'Prabumulih','Sumatera Selatan') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (151,'Muara Enim','Sumatera Selatan'),
	 (152,'Ogan Komering Ulu','Sumatera Selatan'),
	 (153,'Ogan Komering Ulu Selatan','Sumatera Selatan'),
	 (154,'Gorontalo','Gorontalo'),
	 (155,'Boalemo','Gorontalo'),
	 (156,'Bone Bolango','Gorontalo'),
	 (157,'Gorontalo Utara','Gorontalo'),
	 (158,'Pohuwato','Gorontalo'),
	 (159,'Bengkulu','Bengkulu'),
	 (160,'Bengkulu Tengah','Bengkulu') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (161,'Bengkulu Utara','Bengkulu'),
	 (162,'Rejang Lebong','Bengkulu'),
	 (163,'Muko Muko','Bengkulu'),
	 (164,'Kepahiang','Bengkulu'),
	 (165,'Kaur','Bengkulu'),
	 (166,'Bengkulu Selatan','Bengkulu'),
	 (167,'Lebong','Bengkulu'),
	 (168,'Seluma','Bengkulu'),
	 (169,'Poso','Sulawesi Tengah'),
	 (170,'Palu','Sulawesi Tengah') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (171,'Parigi Moutong','Sulawesi Tengah'),
	 (172,'Toli-Toli','Sulawesi Tengah'),
	 (173,'Tojo Una-Una','Sulawesi Tengah'),
	 (174,'Donggala','Sulawesi Tengah'),
	 (175,'Banggai Kepulauan','Sulawesi Tengah'),
	 (176,'Sigi','Sulawesi Tengah'),
	 (177,'Morowali','Sulawesi Tengah'),
	 (178,'Buol','Sulawesi Tengah'),
	 (179,'Banggai','Sulawesi Tengah'),
	 (180,'Bangka','Bangka Belitung') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (181,'Bangka Barat','Bangka Belitung'),
	 (182,'Bangka Selatan','Bangka Belitung'),
	 (183,'Bangka Tengah','Bangka Belitung'),
	 (184,'Belitung','Bangka Belitung'),
	 (185,'Belitung Timur','Bangka Belitung'),
	 (186,'Pangkal Pinang','Bangka Belitung'),
	 (187,'Mamuju Utara','Sulawesi Barat'),
	 (188,'Majene','Sulawesi Barat'),
	 (189,'Polewali Mandar','Sulawesi Barat'),
	 (190,'Mamuju','Sulawesi Barat') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (191,'Mamasa','Sulawesi Barat'),
	 (192,'Pulang Pisau','Kalimantan Tengah'),
	 (193,'Kotawaringin Timur','Kalimantan Tengah'),
	 (194,'Barito Utara','Kalimantan Tengah'),
	 (195,'Kotawaringin Barat','Kalimantan Tengah'),
	 (196,'Lamandau','Kalimantan Tengah'),
	 (197,'Seruyan','Kalimantan Tengah'),
	 (198,'Katingan','Kalimantan Tengah'),
	 (199,'Kapuas','Kalimantan Tengah'),
	 (200,'Murung Raya','Kalimantan Tengah') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (201,'Gunung Mas','Kalimantan Tengah'),
	 (202,'Palangka Raya','Kalimantan Tengah'),
	 (203,'Sukamara','Kalimantan Tengah'),
	 (204,'Barito Selatan','Kalimantan Tengah'),
	 (205,'Barito Timur','Kalimantan Tengah'),
	 (206,'Kepulauan Sula','Maluku Utara'),
	 (207,'Halmahera Barat','Maluku Utara'),
	 (208,'Halmahera Timur','Maluku Utara'),
	 (209,'Tidore Kepulauan','Maluku Utara'),
	 (210,'Pulau Morotai','Maluku Utara') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (211,'Halmahera Tengah','Maluku Utara'),
	 (212,'Ternate','Maluku Utara'),
	 (213,'Halmahera Utara','Maluku Utara'),
	 (214,'Halmahera Selatan','Maluku Utara'),
	 (215,'Batam','Kepulauan Riau'),
	 (216,'Bintan','Kepulauan Riau'),
	 (217,'Karimun','Kepulauan Riau'),
	 (218,'Kepulauan Anambas','Kepulauan Riau'),
	 (219,'Lingga','Kepulauan Riau'),
	 (220,'Natuna','Kepulauan Riau') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (221,'Tanjung Pinang','Kepulauan Riau'),
	 (222,'Pasaman Barat','Sumatera Barat'),
	 (223,'Pasaman','Sumatera Barat'),
	 (224,'Payakumbuh','Sumatera Barat'),
	 (225,'Sawah Lunto','Sumatera Barat'),
	 (226,'Solok','Sumatera Barat'),
	 (227,'Padang','Sumatera Barat'),
	 (228,'Kepulauan Mentawai','Sumatera Barat'),
	 (229,'Agam','Sumatera Barat'),
	 (230,'Dharmasraya','Sumatera Barat') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (231,'Tanah Datar','Sumatera Barat'),
	 (232,'Padang Panjang','Sumatera Barat'),
	 (233,'Pesisir Selatan','Sumatera Barat'),
	 (234,'Sijunjung (Sawah Lunto Sijunjung)','Sumatera Barat'),
	 (235,'Bukittinggi','Sumatera Barat'),
	 (236,'Pariaman','Sumatera Barat'),
	 (237,'Padang Pariaman','Sumatera Barat'),
	 (238,'Lima Puluh Koto/Kota','Sumatera Barat'),
	 (239,'Solok Selatan','Sumatera Barat'),
	 (240,'Karawang','Jawa Barat') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (241,'Pangandaran','Jawa Barat'),
	 (242,'Banjar','Jawa Barat'),
	 (243,'Cimahi','Jawa Barat'),
	 (244,'Cirebon','Jawa Barat'),
	 (245,'Ciamis','Jawa Barat'),
	 (246,'Indramayu','Jawa Barat'),
	 (247,'Bogor','Jawa Barat'),
	 (248,'Kuningan','Jawa Barat'),
	 (249,'Garut','Jawa Barat'),
	 (250,'Bandung','Jawa Barat') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (251,'Majalengka','Jawa Barat'),
	 (252,'Sukabumi','Jawa Barat'),
	 (253,'Cianjur','Jawa Barat'),
	 (254,'Subang','Jawa Barat'),
	 (255,'Depok','Jawa Barat'),
	 (256,'Bekasi','Jawa Barat'),
	 (257,'Purwakarta','Jawa Barat'),
	 (258,'Bandung Barat','Jawa Barat'),
	 (259,'Sumedang','Jawa Barat'),
	 (260,'Tasikmalaya','Jawa Barat') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (261,'Jakarta Barat','DKI JAKARTA'),
	 (262,'Jakarta Pusat','DKI JAKARTA'),
	 (263,'Jakarta Selatan','DKI JAKARTA'),
	 (264,'Jakarta Timur','DKI JAKARTA'),
	 (265,'Jakarta Utara','DKI JAKARTA'),
	 (266,'Kepulauan Seribu','DKI JAKARTA'),
	 (267,'Maybrat','Papua Barat'),
	 (268,'Teluk Bintuni','Papua Barat'),
	 (269,'Manokwari','Papua Barat'),
	 (270,'Fakfak','Papua Barat') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (271,'Sorong','Papua Barat'),
	 (272,'Kaimana','Papua Barat'),
	 (273,'Pegunungan Arfak','Papua Barat'),
	 (274,'Sorong Selatan','Papua Barat'),
	 (275,'Manokwari Selatan','Papua Barat'),
	 (276,'Tambrauw','Papua Barat'),
	 (277,'Teluk Wondama','Papua Barat'),
	 (278,'Raja Ampat','Papua Barat'),
	 (279,'Pontianak','Kalimantan Barat'),
	 (280,'Ketapang','Kalimantan Barat') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (281,'Kayong Utara','Kalimantan Barat'),
	 (282,'Sambas','Kalimantan Barat'),
	 (283,'Sekadau','Kalimantan Barat'),
	 (284,'Sanggau','Kalimantan Barat'),
	 (285,'Bengkayang','Kalimantan Barat'),
	 (286,'Melawi','Kalimantan Barat'),
	 (287,'Landak','Kalimantan Barat'),
	 (288,'Kapuas Hulu','Kalimantan Barat'),
	 (289,'Sintang','Kalimantan Barat'),
	 (290,'Singkawang','Kalimantan Barat') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (291,'Kubu Raya','Kalimantan Barat'),
	 (292,'Waropen','Papua'),
	 (293,'Lanny Jaya','Papua'),
	 (294,'Boven Digoel','Papua'),
	 (295,'Nduga','Papua'),
	 (296,'Asmat','Papua'),
	 (297,'Supiori','Papua'),
	 (298,'Mamberamo Tengah','Papua'),
	 (299,'Mappi','Papua'),
	 (300,'Keerom','Papua') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (301,'Mimika','Papua'),
	 (302,'Jayawijaya','Papua'),
	 (303,'Mamberamo Raya','Papua'),
	 (304,'Biak Numfor','Papua'),
	 (305,'Dogiyai','Papua'),
	 (306,'Yalimo','Papua'),
	 (307,'Merauke','Papua'),
	 (308,'Deiyai (Deliyai)','Papua'),
	 (309,'Yahukimo','Papua'),
	 (310,'Sarmi','Papua') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (311,'Jayapura','Papua'),
	 (312,'Nabire','Papua'),
	 (313,'Paniai','Papua'),
	 (314,'Intan Jaya','Papua'),
	 (315,'Puncak','Papua'),
	 (316,'Tolikara','Papua'),
	 (317,'Pegunungan Bintang','Papua'),
	 (318,'Kepulauan Yapen (Yapen Waropen)','Papua'),
	 (319,'Puncak Jaya','Papua'),
	 (320,'Banjar','Kalimantan Selatan') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (321,'Banjarmasin','Kalimantan Selatan'),
	 (322,'Balangan','Kalimantan Selatan'),
	 (323,'Banjarbaru','Kalimantan Selatan'),
	 (324,'Hulu Sungai Selatan','Kalimantan Selatan'),
	 (325,'Kotabaru','Kalimantan Selatan'),
	 (326,'Tabalong','Kalimantan Selatan'),
	 (327,'Tapin','Kalimantan Selatan'),
	 (328,'Tanah Bumbu','Kalimantan Selatan'),
	 (329,'Barito Kuala','Kalimantan Selatan'),
	 (330,'Tanah Laut','Kalimantan Selatan') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (331,'Hulu Sungai Utara','Kalimantan Selatan'),
	 (332,'Hulu Sungai Tengah','Kalimantan Selatan'),
	 (333,'Bantul','DI Yogyakarta'),
	 (334,'Gunung Kidul','DI Yogyakarta'),
	 (335,'Kulon Progo','DI Yogyakarta'),
	 (336,'Sleman','DI Yogyakarta'),
	 (337,'Yogyakarta','DI Yogyakarta'),
	 (338,'Nunukan','Kalimantan Utara'),
	 (339,'Bulungan (Bulongan)','Kalimantan Utara'),
	 (340,'Malinau','Kalimantan Utara') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (341,'Tana Tidung','Kalimantan Utara'),
	 (342,'Tarakan','Kalimantan Utara'),
	 (343,'Seram Bagian Barat','Maluku'),
	 (344,'Seram Bagian Timur','Maluku'),
	 (345,'Ambon','Maluku'),
	 (346,'Tual','Maluku'),
	 (347,'Maluku Tengah','Maluku'),
	 (348,'Maluku Barat Daya','Maluku'),
	 (349,'Maluku Tenggara Barat','Maluku'),
	 (350,'Buru','Maluku') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (351,'Maluku Tenggara','Maluku'),
	 (352,'Kepulauan Aru','Maluku'),
	 (353,'Buru Selatan','Maluku'),
	 (354,'Bandar Lampung','Lampung'),
	 (355,'Mesuji','Lampung'),
	 (356,'Tulang Bawang Barat','Lampung'),
	 (357,'Lampung Utara','Lampung'),
	 (358,'Tanggamus','Lampung'),
	 (359,'Lampung Selatan','Lampung'),
	 (360,'Way Kanan','Lampung') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (361,'Pesawaran','Lampung'),
	 (362,'Lampung Tengah','Lampung'),
	 (363,'Pringsewu','Lampung'),
	 (364,'Lampung Timur','Lampung'),
	 (365,'Lampung Barat','Lampung'),
	 (366,'Metro','Lampung'),
	 (367,'Pesisir Barat','Lampung'),
	 (368,'Tulang Bawang','Lampung'),
	 (369,'Pekalongan','Jawa Tengah'),
	 (370,'Sukoharjo','Jawa Tengah') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (371,'Banjarnegara','Jawa Tengah'),
	 (372,'Boyolali','Jawa Tengah'),
	 (373,'Semarang','Jawa Tengah'),
	 (374,'Grobogan','Jawa Tengah'),
	 (375,'Salatiga','Jawa Tengah'),
	 (376,'Batang','Jawa Tengah'),
	 (377,'Cilacap','Jawa Tengah'),
	 (378,'Demak','Jawa Tengah'),
	 (379,'Kebumen','Jawa Tengah'),
	 (380,'Brebes','Jawa Tengah') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (381,'Magelang','Jawa Tengah'),
	 (382,'Wonosobo','Jawa Tengah'),
	 (383,'Temanggung','Jawa Tengah'),
	 (384,'Tegal','Jawa Tengah'),
	 (385,'Karanganyar','Jawa Tengah'),
	 (386,'Kudus','Jawa Tengah'),
	 (387,'Kendal','Jawa Tengah'),
	 (388,'Blora','Jawa Tengah'),
	 (389,'Jepara','Jawa Tengah'),
	 (390,'Surakarta (Solo)','Jawa Tengah') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (391,'Klaten','Jawa Tengah'),
	 (392,'Wonogiri','Jawa Tengah'),
	 (393,'Rembang','Jawa Tengah'),
	 (394,'Banyumas','Jawa Tengah'),
	 (395,'Sragen','Jawa Tengah'),
	 (396,'Purworejo','Jawa Tengah'),
	 (397,'Pati','Jawa Tengah'),
	 (398,'Purbalingga','Jawa Tengah'),
	 (399,'Pemalang','Jawa Tengah'),
	 (400,'Gayo Lues','Nanggroe Aceh Darussalam (NAD)') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (401,'Aceh Besar','Nanggroe Aceh Darussalam (NAD)'),
	 (402,'Aceh Utara','Nanggroe Aceh Darussalam (NAD)'),
	 (403,'Nagan Raya','Nanggroe Aceh Darussalam (NAD)'),
	 (404,'Aceh Tengah','Nanggroe Aceh Darussalam (NAD)'),
	 (405,'Lhokseumawe','Nanggroe Aceh Darussalam (NAD)'),
	 (406,'Aceh Selatan','Nanggroe Aceh Darussalam (NAD)'),
	 (407,'Aceh Barat','Nanggroe Aceh Darussalam (NAD)'),
	 (408,'Banda Aceh','Nanggroe Aceh Darussalam (NAD)'),
	 (409,'Simeulue','Nanggroe Aceh Darussalam (NAD)'),
	 (410,'Aceh Singkil','Nanggroe Aceh Darussalam (NAD)') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (411,'Pidie','Nanggroe Aceh Darussalam (NAD)'),
	 (412,'Aceh Barat Daya','Nanggroe Aceh Darussalam (NAD)'),
	 (413,'Aceh Timur','Nanggroe Aceh Darussalam (NAD)'),
	 (414,'Aceh Jaya','Nanggroe Aceh Darussalam (NAD)'),
	 (415,'Aceh Tenggara','Nanggroe Aceh Darussalam (NAD)'),
	 (416,'Pidie Jaya','Nanggroe Aceh Darussalam (NAD)'),
	 (417,'Aceh Tamiang','Nanggroe Aceh Darussalam (NAD)'),
	 (418,'Sabang','Nanggroe Aceh Darussalam (NAD)'),
	 (419,'Langsa','Nanggroe Aceh Darussalam (NAD)'),
	 (420,'Subulussalam','Nanggroe Aceh Darussalam (NAD)') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (421,'Bener Meriah','Nanggroe Aceh Darussalam (NAD)'),
	 (422,'Bireuen','Nanggroe Aceh Darussalam (NAD)'),
	 (423,'Konawe Utara','Sulawesi Tenggara'),
	 (424,'Konawe Selatan','Sulawesi Tenggara'),
	 (425,'Kendari','Sulawesi Tenggara'),
	 (426,'Kolaka Utara','Sulawesi Tenggara'),
	 (427,'Konawe','Sulawesi Tenggara'),
	 (428,'Wakatobi','Sulawesi Tenggara'),
	 (429,'Buton','Sulawesi Tenggara'),
	 (430,'Bau-Bau','Sulawesi Tenggara') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (431,'Muna','Sulawesi Tenggara'),
	 (432,'Bombana','Sulawesi Tenggara'),
	 (433,'Kolaka','Sulawesi Tenggara'),
	 (434,'Buton Utara','Sulawesi Tenggara'),
	 (435,'Minahasa Selatan','Sulawesi Utara'),
	 (436,'Kepulauan Siau Tagulandang Biaro (Sitaro)','Sulawesi Utara'),
	 (437,'Minahasa','Sulawesi Utara'),
	 (438,'Kotamobagu','Sulawesi Utara'),
	 (439,'Bolaang Mongondow (Bolmong)','Sulawesi Utara'),
	 (440,'Manado','Sulawesi Utara') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (441,'Bolaang Mongondow Selatan','Sulawesi Utara'),
	 (442,'Minahasa Utara','Sulawesi Utara'),
	 (443,'Bolaang Mongondow Timur','Sulawesi Utara'),
	 (444,'Minahasa Tenggara','Sulawesi Utara'),
	 (445,'Bolaang Mongondow Utara','Sulawesi Utara'),
	 (446,'Kepulauan Talaud','Sulawesi Utara'),
	 (447,'Kepulauan Sangihe','Sulawesi Utara'),
	 (448,'Tomohon','Sulawesi Utara'),
	 (449,'Bitung','Sulawesi Utara'),
	 (450,'Timor Tengah Selatan','Nusa Tenggara Timur (NTT)') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (451,'Lembata','Nusa Tenggara Timur (NTT)'),
	 (452,'Sumba Tengah','Nusa Tenggara Timur (NTT)'),
	 (453,'Manggarai Timur','Nusa Tenggara Timur (NTT)'),
	 (454,'Alor','Nusa Tenggara Timur (NTT)'),
	 (455,'Sumba Barat','Nusa Tenggara Timur (NTT)'),
	 (456,'Manggarai Barat','Nusa Tenggara Timur (NTT)'),
	 (457,'Sikka','Nusa Tenggara Timur (NTT)'),
	 (458,'Timor Tengah Utara','Nusa Tenggara Timur (NTT)'),
	 (459,'Nagekeo','Nusa Tenggara Timur (NTT)'),
	 (460,'Ngada','Nusa Tenggara Timur (NTT)') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (461,'Manggarai','Nusa Tenggara Timur (NTT)'),
	 (462,'Sabu Raijua','Nusa Tenggara Timur (NTT)'),
	 (463,'Ende','Nusa Tenggara Timur (NTT)'),
	 (464,'Flores Timur','Nusa Tenggara Timur (NTT)'),
	 (465,'Kupang','Nusa Tenggara Timur (NTT)'),
	 (466,'Belu','Nusa Tenggara Timur (NTT)'),
	 (467,'Sumba Timur','Nusa Tenggara Timur (NTT)'),
	 (468,'Rote Ndao','Nusa Tenggara Timur (NTT)'),
	 (469,'Sumba Barat Daya','Nusa Tenggara Timur (NTT)'),
	 (470,'Tangerang Selatan','Banten') ON DUPLICATE KEY UPDATE id = VALUES (id);
INSERT INTO cities (id,name,province_name) VALUES
	 (471,'Pandeglang','Banten'),
	 (472,'Cilegon','Banten'),
	 (473,'Lebak','Banten'),
	 (474,'Serang','Banten'),
	 (475,'Tangerang','Banten') ON DUPLICATE KEY UPDATE id = VALUES (id);
`;

export default seeds;
