import cors from "cors";
import "dotenv/config";
import express from "express";
import morgan from "morgan";
import ResMsg from "./assets/string/ResponseMessage.json";
import Logger from "./helpers/Logger";
import Response from "./helpers/Response";
import LoginCheck from "./middlewares/LoginCheck";
import RoleValidator from "./middlewares/RoleValidator";
import SchoolPeriodCheck from "./middlewares/SchoolPeriodCheck";

const app = express();
const port = process.env.PORT || 3000;

app.use(
  cors(),
  express.json({
    limit: "10mb",
  }),
  express.urlencoded({
    limit: "10mb",
    extended: true,
  }),
);

if (process.env.NODE_ENV !== "production") app.use(morgan("dev"));

app.use(require("./routes/Home"));
app.use(require("./routes/Test"));
app.use(require("./routes/Auth"));
app.use(require("./routes/City"));

// route with login n role check
app.use(LoginCheck);
app.use((req, res, next) => {
  RoleValidator(req, res, next, []);
});

app.use(require("./routes/History"));
app.use(require("./routes/Role"));
app.use(require("./routes/ClassMajor"));
app.use(require("./routes/SchoolPeriod"));

// route with period check
app.use(SchoolPeriodCheck);

app.use(require("./routes/User"));
app.use(require("./routes/Class"));
app.use(require("./routes/ClassSchedule"));
app.use(require("./routes/ClassSession"));
app.use(require("./routes/SchoolAttendence"));
app.use(require("./routes/SchoolBill"));
app.use(require("./routes/Competency"));
app.use(require("./routes/CompetencyScore"));
app.use(require("./routes/Subject"));
app.use(require("./routes/Extracurricular"));
app.use(require("./routes/Attitude"));
app.use(require("./routes/HomeroomTeacherNote"));
app.use(require("./routes/ExtracurricularScore"));
app.use(require("./routes/Achievement"));
app.use(require("./routes/HomeroomTeacherNoteResult"));
app.use(require("./routes/AttitudeReport"));
app.use(require("./routes/Report"));

app.use(function (error, req, res, next) {
  if (error.name == "UnauthorizedError") {
    return res
      .status(ResMsg.auth.invalidToken.code)
      .json(Response.error(ResMsg.auth.invalidToken.message));
  } else if (process.env.NODE_ENV == "production") {
    Logger.error(error.error);
    Logger.info(
      JSON.stringify({
        fun: error.fun,
        user: req.auth ?? {},
        body: req.body ?? {},
      })
    );
  } else {
    console.log(error);
  }

  return res
    .status(ResMsg.general.generalError.code)
    .json(Response.error(ResMsg.general.generalError.message));
});

app.use(function (req, res, next) {
  return res
    .status(ResMsg.general.pageNotFound.code)
    .json(Response.error(ResMsg.general.pageNotFound.message));
});

app.listen(port, () => {
  console.log(`Saais Backend Server started on port ${port}`);
});

export default app;
