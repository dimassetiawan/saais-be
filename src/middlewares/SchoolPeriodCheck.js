import { Router } from "express";
import ResMsg from "../assets/string/ResponseMessage.json";
import RedisClient from "../helpers/Redis";
import Response from "../helpers/Response";
import { school_periods } from "../models";

const routes = Router();

routes.use(async function (req, res, next) {
  try {
    // set req.settint.period_id value

    req.setting = {};
    req.setting.period_id = await RedisClient.get("SaaisSetting:SchoolPeriod");

    if (!req.setting.period_id) {
      let period = await school_periods.findOne({
        attribute: ["id"],
        where: {
          is_active: true,
        },
        order: [["created_at", "desc"]],
      });

      if (period) {
        req.setting.period_id = period.id;
        await RedisClient.set("SaaisSetting:SchoolPeriod", period.id);
        await RedisClient.expire("SaaisSetting:SchoolPeriod", 60 * 60 * 24);
      } else {
        return res
          .status(ResMsg.period.periodNotSet.code)
          .json(Response.error(ResMsg.period.periodNotSet));
      }
    }
    next();
  } catch (error) {
    next({ error, fun: "SchoolPeriodCheck" });
  }
});

module.exports = routes;
