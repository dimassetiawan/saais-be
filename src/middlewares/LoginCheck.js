import { expressjwt } from "express-jwt";
import RedisClient from "../helpers/Redis";

export default expressjwt({
  secret: process.env.JWT_SECRET_KEY,
  algorithms: ["HS256"],
  isRevoked: async (req, user, done) => {
    let userData = await RedisClient.get("SaaisUserData:" + user.payload.id);
    userData = JSON.parse(userData);

    if (!userData || userData?.hash != user.payload.jti) {
      return true;
    } else {
      return false;
    }
  },
});
