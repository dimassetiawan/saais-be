import ResMsg from "../assets/string/ResponseMessage.json";
import Response from "../helpers/Response";

export default async function roleValidator(req, res, next, role) {
  let isAllowed = false;

  if (req.auth?.user_roles?.length > 0 && role.length > 0) {
    for await (let item of req.auth.user_roles) {
      if (role.includes(item.code) > 0) {
        isAllowed = true;
        break;
      }
    }
  } else if (role.length <= 0) {
    isAllowed = true;
  }

  if (isAllowed) {
    next();
  } else {
    return res
      .status(ResMsg.auth.invalidAccess.code)
      .json(Response.error(ResMsg.auth.invalidAccess.message));
  }

}
