import crypto from "crypto";

const algorithm = "aes-128-cbc";
const encryptionKey = process.env.AES_KEY;
const ivLength = 16;

class Encryptor {
  static async encrypt(text) {
    try {
      let iv = crypto.randomBytes(ivLength);
      let cipher = crypto.createCipheriv(
        algorithm,
        Buffer.from(encryptionKey, "utf-8"),
        iv
      );
      let encrypted = cipher.update(text);
      encrypted = Buffer.concat([encrypted, cipher.final()]);
      return iv.toString("base64") + ":" + encrypted.toString("base64");
    } catch (error) {
      return "";
    }
  }

  static async decrypt(text) {
    try {
      let textParts = text.split(":");
      let iv = Buffer.from(textParts.shift(), "base64");
      let encryptedText = Buffer.from(textParts.join(":"), "base64");
      let decipher = crypto.createDecipheriv(
        algorithm,
        Buffer.from(encryptionKey, "utf-8"),
        iv
      );
      let decrypted = decipher.update(encryptedText);
      decrypted = Buffer.concat([decrypted, decipher.final()]);
      return decrypted.toString();
    } catch (error) {
      return "";
    }
  }
}
module.exports = Encryptor;
