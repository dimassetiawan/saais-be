const pageSize = 10;

class Paging {
  static pagingUtil(req) {
    let page =
      parseInt(req.query.page) || parseInt(req.query.page) > 0
        ? parseInt(req.query.page)
        : 1;
    let perPage = req.query.perPage ? parseInt(req.query.perPage) : pageSize;

    return {
      page,
      perPage,
    };
  }
}

module.exports = Paging;
