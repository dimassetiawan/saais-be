import jwt from "jsonwebtoken";
import md5 from "md5";
import { Sequelize } from "sequelize";
import RedisClient from "../helpers/Redis";
import { cities, roles, user_details, user_roles, users } from "../models";

class UserUtil {
  static async getUserData(username, id, createNew) {
    let user;

    if (!username && !id) {
      return user;
    }

    if (id && !createNew) {
      // note: always create new cache if only username provided
      user = await RedisClient.get("SaaisUserData:" + id);
      if (user) {
        user = JSON.parse(user);
      }
    }

    if (!user || createNew) {
      let whereQuery = {};

      if (username) {
        whereQuery.username = username;
      }

      if (id) {
        whereQuery.id = id;
      }

      user = await users.findOne({
        where: whereQuery,
        attributes: {
          exclude: ["created_at", "updated_at"],
        },
        include: [
          {
            model: user_details,
            required: false,
            attributes: {
              exclude: ["created_at", "updated_at"],
              include: [
                [Sequelize.literal("`user_detail->city`.name"), "birth_city_name"],
                [
                  Sequelize.literal("`user_detail->city`.province_name"),
                  "birth_city_province_name",
                ],
              ],
            },
            include: {
              model: cities,
              required: false,
              attributes: [],
            },
          },
          {
            model: user_roles,
            required: false,
            separate: true,
            attributes: ["code", [Sequelize.col("role.name"), "name"]],
            include: {
              model: roles,
              attributes: [],
            },
          },
        ],
      });

      if (user) {
        user = user.toJSON();
      } else {
        return;
      }

      // create hash to validate token
      user.hash = md5(JSON.stringify(user));

      let userWithoutPass = { ...user };
      delete userWithoutPass.password;

      // set cache in redis, used for validate token
      await RedisClient.set(
        "SaaisUserData:" + user.id,
        JSON.stringify(userWithoutPass)
      );
      await RedisClient.expire("SaaisUserData:" + user.id, 60 * 60 * 24);
    }

    return user;
  }

  static async getToken(user) {
    let token = jwt.sign(
      {
        id: user.id,
        username: user.username,
        user_roles: user.user_roles
      },
      process.env.JWT_SECRET_KEY,
      {
        algorithm: "HS256",
        jwtid: user.hash,
      }
    );

    return token;
  }
}

module.exports = UserUtil;
