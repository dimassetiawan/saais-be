import joi from "joi";
import { histories } from "../models";

class History {
  static async createHistory(historyData) {
    const schema = joi
      .array()
      .items(
        joi
          .object()
          .keys({
            action: joi.string().trim().required(),
            table_name: joi.string().max(100).trim().required(),
            table_id: joi.any().optional(),
            created_by: joi.number().required(),
            previous_data: joi.any().optional(),
          })
          .required()
      )
      .required();

    const validate = schema.validate(historyData);

    if (validate.error) {
      throw new Error("Invalid history data, " + validate.error.message);
    }

    await histories.bulkCreate(historyData);
  }
}

module.exports = History;
