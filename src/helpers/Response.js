import ResMsg from "../assets/string/ResponseMessage.json";

class Response {
  static error(message) {
    return {
      message: message,
      status: false,
    };
  }

  static success(data, message) {
    return {
      message: message ?? ResMsg.general.operationSuccess,
      data: data,
      status: true,
    };
  }

  static indexPaging(counter, paging, data) {
    let lastPage = Math.ceil(counter / paging.perPage);
    let from = paging.page * paging.perPage - paging.perPage + 1;
    let to = paging.page * paging.perPage;

    return {
      status: true,
      total: counter,
      lastPage: lastPage,
      perPage: paging.perPage,
      currentPage: paging.page,
      from: from,
      to: to,
      data: data,
    };
  }
}

module.exports = Response;
