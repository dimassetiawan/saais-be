var DataTypes = require("sequelize").DataTypes;
var _achievements = require("./achievements");
var _attitude_score_details = require("./attitude_score_details");
var _attitude_scores = require("./attitude_scores");
var _attitudes = require("./attitudes");
var _cities = require("./cities");
var _class_attendances = require("./class_attendances");
var _class_majors = require("./class_majors");
var _class_schedules = require("./class_schedules");
var _class_sessions = require("./class_sessions");
var _classes = require("./classes");
var _competencies = require("./competencies");
var _competency_scores = require("./competency_scores");
var _histories = require("./histories");
var _homeroom_teacher_note_results = require("./homeroom_teacher_note_results");
var _homeroom_teacher_notes = require("./homeroom_teacher_notes");
var _nonacademic_subject_scores = require("./nonacademic_subject_scores");
var _nonacademic_subjects = require("./nonacademic_subjects");
var _roles = require("./roles");
var _school_attendances = require("./school_attendances");
var _school_bill_payments = require("./school_bill_payments");
var _school_bills = require("./school_bills");
var _school_periods = require("./school_periods");
var _settings = require("./settings");
var _student_classes = require("./student_classes");
var _subjects = require("./subjects");
var _user_details = require("./user_details");
var _user_roles = require("./user_roles");
var _users = require("./users");

function initModels(sequelize) {
  var achievements = _achievements(sequelize, DataTypes);
  var attitude_score_details = _attitude_score_details(sequelize, DataTypes);
  var attitude_scores = _attitude_scores(sequelize, DataTypes);
  var attitudes = _attitudes(sequelize, DataTypes);
  var cities = _cities(sequelize, DataTypes);
  var class_attendances = _class_attendances(sequelize, DataTypes);
  var class_majors = _class_majors(sequelize, DataTypes);
  var class_schedules = _class_schedules(sequelize, DataTypes);
  var class_sessions = _class_sessions(sequelize, DataTypes);
  var classes = _classes(sequelize, DataTypes);
  var competencies = _competencies(sequelize, DataTypes);
  var competency_scores = _competency_scores(sequelize, DataTypes);
  var histories = _histories(sequelize, DataTypes);
  var homeroom_teacher_note_results = _homeroom_teacher_note_results(sequelize, DataTypes);
  var homeroom_teacher_notes = _homeroom_teacher_notes(sequelize, DataTypes);
  var nonacademic_subject_scores = _nonacademic_subject_scores(sequelize, DataTypes);
  var nonacademic_subjects = _nonacademic_subjects(sequelize, DataTypes);
  var roles = _roles(sequelize, DataTypes);
  var school_attendances = _school_attendances(sequelize, DataTypes);
  var school_bill_payments = _school_bill_payments(sequelize, DataTypes);
  var school_bills = _school_bills(sequelize, DataTypes);
  var school_periods = _school_periods(sequelize, DataTypes);
  var settings = _settings(sequelize, DataTypes);
  var student_classes = _student_classes(sequelize, DataTypes);
  var subjects = _subjects(sequelize, DataTypes);
  var user_details = _user_details(sequelize, DataTypes);
  var user_roles = _user_roles(sequelize, DataTypes);
  var users = _users(sequelize, DataTypes);

  classes.belongsToMany(users, { as: 'student_id_users', through: student_classes, foreignKey: "class_id", otherKey: "student_id" });
  roles.belongsToMany(users, { as: 'user_id_users', through: user_roles, foreignKey: "code", otherKey: "user_id" });
  users.belongsToMany(classes, { as: 'class_id_classes', through: student_classes, foreignKey: "student_id", otherKey: "class_id" });
  users.belongsToMany(roles, { as: 'code_roles', through: user_roles, foreignKey: "user_id", otherKey: "code" });
  attitude_score_details.belongsTo(attitude_scores, { foreignKey: "attitude_score_id"});
  attitude_scores.hasMany(attitude_score_details, { foreignKey: "attitude_score_id"});
  attitude_score_details.belongsTo(attitudes, { foreignKey: "attitude_id"});
  attitudes.hasMany(attitude_score_details, { foreignKey: "attitude_id"});
  user_details.belongsTo(cities, { foreignKey: "place_of_birth_id"});
  cities.hasMany(user_details, { foreignKey: "place_of_birth_id"});
  classes.belongsTo(class_majors, { foreignKey: "major_id"});
  class_majors.hasMany(classes, { foreignKey: "major_id"});
  class_attendances.belongsTo(class_sessions, { foreignKey: "class_session_id"});
  class_sessions.hasMany(class_attendances, { foreignKey: "class_session_id"});
  student_classes.belongsTo(classes, { foreignKey: "class_id"});
  classes.hasMany(student_classes, { foreignKey: "class_id"});
  subjects.belongsTo(classes, { foreignKey: "class_id"});
  classes.hasMany(subjects, { foreignKey: "class_id"});
  competency_scores.belongsTo(competencies, { foreignKey: "competency_id"});
  competencies.hasMany(competency_scores, { foreignKey: "competency_id"});
  homeroom_teacher_note_results.belongsTo(homeroom_teacher_notes, { foreignKey: "note_id"});
  homeroom_teacher_notes.hasMany(homeroom_teacher_note_results, { foreignKey: "note_id"});
  nonacademic_subject_scores.belongsTo(nonacademic_subjects, { foreignKey: "nonacademic_subject_id"});
  nonacademic_subjects.hasMany(nonacademic_subject_scores, { foreignKey: "nonacademic_subject_id"});
  user_roles.belongsTo(roles, { foreignKey: "code"});
  roles.hasMany(user_roles, { foreignKey: "code"});
  school_bill_payments.belongsTo(school_bills, { foreignKey: "school_bill_id"});
  school_bills.hasMany(school_bill_payments, { foreignKey: "school_bill_id"});
  achievements.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(achievements, { foreignKey: "period_id"});
  attitude_scores.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(attitude_scores, { foreignKey: "period_id"});
  attitudes.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(attitudes, { foreignKey: "period_id"});
  classes.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(classes, { foreignKey: "period_id"});
  homeroom_teacher_note_results.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(homeroom_teacher_note_results, { foreignKey: "period_id"});
  homeroom_teacher_notes.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(homeroom_teacher_notes, { foreignKey: "period_id"});
  nonacademic_subjects.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(nonacademic_subjects, { foreignKey: "period_id"});
  school_attendances.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(school_attendances, { foreignKey: "period_id"});
  school_bills.belongsTo(school_periods, { foreignKey: "period_id"});
  school_periods.hasMany(school_bills, { foreignKey: "period_id"});
  class_schedules.belongsTo(subjects, { foreignKey: "subject_id"});
  subjects.hasMany(class_schedules, { foreignKey: "subject_id"});
  class_sessions.belongsTo(subjects, { foreignKey: "subject_id"});
  subjects.hasMany(class_sessions, { foreignKey: "subject_id"});
  competencies.belongsTo(subjects, { foreignKey: "subject_id"});
  subjects.hasMany(competencies, { foreignKey: "subject_id"});
  achievements.belongsTo(users, { foreignKey: "user_id"});
  users.hasMany(achievements, { foreignKey: "user_id"});
  attitude_scores.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(attitude_scores, { foreignKey: "student_id"});
  class_attendances.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(class_attendances, { foreignKey: "student_id"});
  class_sessions.belongsTo(users, { foreignKey: "teacher_id"});
  users.hasMany(class_sessions, { foreignKey: "teacher_id"});
  classes.belongsTo(users, { foreignKey: "teacher_id"});
  users.hasMany(classes, { foreignKey: "teacher_id"});
  competency_scores.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(competency_scores, { foreignKey: "student_id"});
  histories.belongsTo(users, { foreignKey: "created_by"});
  users.hasMany(histories, { foreignKey: "created_by"});
  homeroom_teacher_note_results.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(homeroom_teacher_note_results, { foreignKey: "student_id"});
  nonacademic_subject_scores.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(nonacademic_subject_scores, { foreignKey: "student_id"});
  nonacademic_subjects.belongsTo(users, { foreignKey: "coach_id"});
  users.hasMany(nonacademic_subjects, { foreignKey: "coach_id"});
  school_attendances.belongsTo(users, { foreignKey: "user_id"});
  users.hasMany(school_attendances, { foreignKey: "user_id"});
  school_bill_payments.belongsTo(users, { foreignKey: "created_by"});
  users.hasMany(school_bill_payments, { foreignKey: "created_by"});
  school_bills.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(school_bills, { foreignKey: "student_id"});
  student_classes.belongsTo(users, { foreignKey: "student_id"});
  users.hasMany(student_classes, { foreignKey: "student_id"});
  subjects.belongsTo(users, { foreignKey: "teacher_id"});
  users.hasMany(subjects, { foreignKey: "teacher_id"});
  user_details.belongsTo(users, { foreignKey: "user_id"});
  users.hasOne(user_details, { foreignKey: "user_id"});
  user_roles.belongsTo(users, { foreignKey: "user_id"});
  users.hasMany(user_roles, { foreignKey: "user_id"});

  return {
    achievements,
    attitude_score_details,
    attitude_scores,
    attitudes,
    cities,
    class_attendances,
    class_majors,
    class_schedules,
    class_sessions,
    classes,
    competencies,
    competency_scores,
    histories,
    homeroom_teacher_note_results,
    homeroom_teacher_notes,
    nonacademic_subject_scores,
    nonacademic_subjects,
    roles,
    school_attendances,
    school_bill_payments,
    school_bills,
    school_periods,
    settings,
    student_classes,
    subjects,
    user_details,
    user_roles,
    users,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
