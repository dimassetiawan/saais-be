const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('school_attendances', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    period_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'school_periods',
        key: 'id'
      }
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    clock_in_date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    clock_in_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('now')
    },
    clock_out_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_late: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
    type: {
      type: DataTypes.ENUM('h','a','i','s'),
      allowNull: false
    },
    clock_in_pic: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    clock_out_pic: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    reason: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'school_attendances',
    timestamps: true,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "school_attendances_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "period_id" },
          { name: "user_id" },
          { name: "clock_in_date" },
        ]
      },
      {
        name: "school_attendances_period_id",
        using: "BTREE",
        fields: [
          { name: "period_id" },
        ]
      },
      {
        name: "school_attendances_user_id",
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
      {
        name: "school_attendances_is_late",
        using: "BTREE",
        fields: [
          { name: "is_late" },
        ]
      },
      {
        name: "school_attendances_type",
        using: "BTREE",
        fields: [
          { name: "type" },
        ]
      },
      {
        name: "school_attendances_clock_in_date",
        using: "BTREE",
        fields: [
          { name: "clock_in_date" },
        ]
      },
    ]
  });
};
