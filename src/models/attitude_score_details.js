const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('attitude_score_details', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    attitude_score_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'attitude_scores',
        key: 'id'
      }
    },
    attitude_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'attitudes',
        key: 'id'
      }
    },
    type: {
      type: DataTypes.ENUM('always_done','improve'),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'attitude_score_details',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "attitude_score_details_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "attitude_score_id" },
          { name: "attitude_id" },
          { name: "type" },
        ]
      },
      {
        name: "attitude_score_details_attitude_score_id",
        using: "BTREE",
        fields: [
          { name: "attitude_score_id" },
        ]
      },
      {
        name: "attitude_score_details_attitude_id",
        using: "BTREE",
        fields: [
          { name: "attitude_id" },
        ]
      },
    ]
  });
};
