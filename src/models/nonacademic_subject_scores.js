const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nonacademic_subject_scores', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nonacademic_subject_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nonacademic_subjects',
        key: 'id'
      }
    },
    student_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    grade: {
      type: DataTypes.ENUM('SB','B','C','K'),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'nonacademic_subject_scores',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "nonacademic_subject_scores_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "student_id" },
          { name: "nonacademic_subject_id" },
        ]
      },
      {
        name: "nonacademic_subject_scores_student_id",
        using: "BTREE",
        fields: [
          { name: "student_id" },
        ]
      },
      {
        name: "nonacademic_subject_scores_nonacademic_subject_id",
        using: "BTREE",
        fields: [
          { name: "nonacademic_subject_id" },
        ]
      },
    ]
  });
};
