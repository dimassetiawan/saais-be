import CLS from "cls-hooked";
import Sequelize from "sequelize";

// used for unmanaged db transaction
const namespace = CLS.createNamespace("saais-namespace");
Sequelize.useCLS(namespace);
const initModel = require("./init-models");

let db = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASS,
  {
    host: process.env.DB_HOST,
    schema: process.env.DB_SCHEMA,
    dialect: "mysql",
    timezone: "+07:00",
    logging: process.env.NODE_ENV == "production" ? false : false,
    dialectOptions: {
      multipleStatements: true,
      dateStrings: true,
      typeCast: function (field, next) { 
        if (field.type === 'DATETIME') {
          return field.string()
        }
        return next()
      },
    },
  }
);

const model = initModel(db);

model.db = db;

module.exports = model;
