const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('attitude_scores', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    period_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'school_periods',
        key: 'id'
      }
    },
    student_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    type: {
      type: DataTypes.ENUM('spiritual','sosial'),
      allowNull: false
    },
    grade: {
      type: DataTypes.ENUM('SB','B','C','K'),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'attitude_scores',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "attitude_scores_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "student_id" },
          { name: "period_id" },
          { name: "type" },
        ]
      },
      {
        name: "attitude_scores_period_id",
        using: "BTREE",
        fields: [
          { name: "period_id" },
        ]
      },
      {
        name: "attitude_scores_student_id",
        using: "BTREE",
        fields: [
          { name: "student_id" },
        ]
      },
    ]
  });
};
