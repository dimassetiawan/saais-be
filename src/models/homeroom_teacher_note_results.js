const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('homeroom_teacher_note_results', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    period_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'school_periods',
        key: 'id'
      }
    },
    student_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    note_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'homeroom_teacher_notes',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'homeroom_teacher_note_results',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "homeroom_teacher_note_results_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "student_id" },
          { name: "period_id" },
          { name: "note_id" },
        ]
      },
      {
        name: "homeroom_teacher_note_results_student_id",
        using: "BTREE",
        fields: [
          { name: "student_id" },
        ]
      },
      {
        name: "homeroom_teacher_note_results_period_id",
        using: "BTREE",
        fields: [
          { name: "period_id" },
        ]
      },
      {
        name: "note_id",
        using: "BTREE",
        fields: [
          { name: "note_id" },
        ]
      },
    ]
  });
};
