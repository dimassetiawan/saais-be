const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user_details', {
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    nisn: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: "nisn"
    },
    nik: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: "nik"
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    place_of_birth_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'cities',
        key: 'id'
      }
    },
    date_of_birth: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    gender: {
      type: DataTypes.ENUM('male', 'female'),
      allowNull: true
    },
    religion: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    education: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    phone_num: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_in_family: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    order_in_family: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    origin_school: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    join_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    join_class: {
      type: DataTypes.ENUM('x', 'xi', 'xii'),
      allowNull: true
    },
    father_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    father_occupation: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    mother_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    mother_occupation: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    parent_phone_num: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    parent_address: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    alumni_education: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    alumni_job: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    graduated_at: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'user_details',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
      {
        name: "nisn",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "nisn" },
        ]
      },
      {
        name: "nik",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "nik" },
        ]
      },
      {
        name: "user_details_gender",
        using: "BTREE",
        fields: [
          { name: "gender" },
        ]
      },
      {
        name: "place_of_birth_id",
        using: "BTREE",
        fields: [
          { name: "place_of_birth_id" },
        ]
      },
    ]
  });
};
