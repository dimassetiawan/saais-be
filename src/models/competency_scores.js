const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('competency_scores', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    competency_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'competencies',
        key: 'id'
      }
    },
    student_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    score: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'competency_scores',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "competency_scores_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "competency_id" },
          { name: "student_id" },
        ]
      },
      {
        name: "competency_scores_competency_id",
        using: "BTREE",
        fields: [
          { name: "competency_id" },
        ]
      },
      {
        name: "competency_scores_student_id",
        using: "BTREE",
        fields: [
          { name: "student_id" },
        ]
      },
    ]
  });
};
