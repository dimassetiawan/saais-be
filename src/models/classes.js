const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('classes', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    period_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'school_periods',
        key: 'id'
      }
    },
    teacher_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "homeroom teacher",
      references: {
        model: 'users',
        key: 'id'
      }
    },
    major_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'class_majors',
        key: 'id'
      }
    },
    grade: {
      type: DataTypes.ENUM('x','xi','xii'),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'classes',
    timestamps: false,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "classes_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "period_id" },
          { name: "major_id" },
          { name: "grade" },
          { name: "name" },
        ]
      },
      {
        name: "classes_period_id",
        using: "BTREE",
        fields: [
          { name: "period_id" },
        ]
      },
      {
        name: "classes_teacher_id",
        using: "BTREE",
        fields: [
          { name: "teacher_id" },
        ]
      },
      {
        name: "classes_major_id",
        using: "BTREE",
        fields: [
          { name: "major_id" },
        ]
      },
      {
        name: "classes_grade",
        using: "BTREE",
        fields: [
          { name: "grade" },
        ]
      },
    ]
  });
};
