import joi from "joi";
import { Op, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  db,
  nonacademic_subject_scores,
  nonacademic_subjects,
  user_roles,
  users
} from "../models";

class ExtracurricularController {

  static async findOne(req, res, next) {
    try {
      let extracurricular = await nonacademic_subjects.findOne({
        where: {
          id: req.params.id
        },
        include: [
          {
            model: users,
            attributes: ["id", "name"]
          },
          {
            model: nonacademic_subject_scores
          }
        ],
      });

      if (!extracurricular) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(extracurricular, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "Extracurricular:findOne" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        where: {
          period_id: req.query.period_id
        },
        include: [
          {
            model: users,
            attributes: ["id", "name"]
          },
          {
            model: nonacademic_subject_scores
          }
        ],
        order: [["name", "asc"]],
      };

      if (req.query.search) {
        query.where = {
          ...query.where,
          name: {
            [Op.like]: "%" + req.query.search + "%"
          }
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      let { count, rows } = await nonacademic_subjects.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "Extracurricular:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        name: joi.string().min(4).max(100).required(),
        coach_id: joi.number().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;
      // validate coach
      if (req.body.coach_id) {
        let coach = await users.findOne({
          where: {
            id: req.body.coach_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "teacher",
            },
          },
        });

        if (!coach) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // create nonacademic_subject
        await nonacademic_subjects.create(req.body);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Extracurricular:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = joi.object().keys({
        name: joi.string().min(4).max(100).required(),
        coach_id: joi.number().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate coach
      if (req.body.coach) {
        let coach = await users.findOne({
          where: {
            id: req.body.coach_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "teacher",
            },
          },
        });

        if (!coach) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // update nonacademic_subjects
        await nonacademic_subjects.update(
          req.body,
          {
            where: {
              id: req.params.id,
            },
          }
        );


        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Extracurricular:update" });
    }
  }

  static async destroy(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing extracurricular
        let extracurricular = await nonacademic_subjects.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!extracurricular) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete extracurricular
        await extracurricular.destroy();

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Extracurricular:destroy" });
    }
  }



}

module.exports = ExtracurricularController;
