import joi from "joi";
import { UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import { classes, competencies, db, subjects } from "../models";

class CompetencyController {
  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        include: [
          {
            model: subjects,
            attributes: [],
            required: true,
            include: {
              model: classes,
              attributes: [],
              required: true,
              where: {
                period_id: req.query.period_id,
              },
            },
          },
        ],
        order: [["code", "asc"]],
      };

      
      if (req.query.subject_id > 0) {
        query.where = {
          ...query.where,
          subject_id: req.query.subject_id,
        };
      }
      
      if (["knowledge", "skill"].includes(req.query.type)) {
        query.where = {
          ...query.where,
          type: req.query.type,
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await competencies.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "Competency:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        class_grade: joi.string().valid("x", "xi", "xii").required(),
        class_major_id: joi.number().required(),
        class_ids: joi.array().items(joi.number().optional()).optional(),
        subject_code: joi.string().required(),
        type: joi.string().valid("knowledge", "skill").required(),
        code: joi.string().max(10).required(),
        description: joi.string().max(100).required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        let subjectQuery = {
          where: {
            code: req.body.subject_code,
          },
          include: {
            model: classes,
            attributes: [],
            required: true,
            where: {
              major_id: req.body.class_major_id,
              grade: req.body.class_grade,
              period_id: req.setting.period_id,
            },
          },
          lock: true,
        };

        if (req.body.class_id?.length > 0) {
          subjectQuery.where = {
            ...subjectQuery.where,
            id: req.body.class_ids,
          };
        }

        // search subject with the criteria
        let currentSubjects = await subjects.findAll(subjectQuery);

        if (currentSubjects.length <= 0) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        let competencyData = [];

        for await (let item of currentSubjects) {
          competencyData.push({
            subject_id: item.id,
            ...req.body,
          });
        }

        // create competency
        await competencies.bulkCreate(competencyData);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Competency:create" });
    }
  }

  static async destroy(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing bill
        let competency = await competencies.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!competency) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete competency
        await competency.destroy();

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Competency:destroy" });
    }
  }
}

module.exports = CompetencyController;
