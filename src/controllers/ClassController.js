import joi from "joi";
import { Op, Sequelize, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  class_majors,
  classes,
  db,
  student_classes,
  user_roles,
  users
} from "../models";

class ClassController {
  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        class_count: joi.number().required(),
        major_id: joi.number().required(),
        grade: joi.string().valid("x", "xi", "xii").required(),
        force_regenerate: joi.boolean().optional()
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;

      await db.transaction(async (dbTrx) => {
        let counterStart = 0;

        if(req.body.force_regenerate){
          await classes.destroy({
            where:{
              major_id: req.body.major_id,
              grade: req.body.grade,
              period_id: req.body.period_id
            },
          })
        }else{
          let lastClass = await classes.findOne({
            where:{
              major_id: req.body.major_id,
              grade: req.body.grade,
              period_id: req.body.period_id
            },
            order:[["name","desc"]]
          })

          counterStart = parseInt(lastClass?.name ?? 0) ?? 0;
        }

        let classData = [];

        for (let index = counterStart + 1; index <= req.body.class_count + counterStart; index++) {
          classData.push({
            ...req.body,
            name: index,
          });
        }

        // create class
        await classes.bulkCreate(classData);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }else if(error.name ==  "SequelizeForeignKeyConstraintError"){
        return res
          .status(ResMsg.general.referenceChildError.code)
          .json(Response.error(ResMsg.general.referenceChildError.message));
      }
      next({ error, fun: "Class:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = joi.object().keys({
        teacher_id: joi.number().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate teacher
      let teacher = await users.findOne({
        where: {
          id: req.body.teacher_id,
        },
        attributes: ["name"],
        include: {
          model: user_roles,
          required: true,
          where: {
            code: "homeroom_teacher",
          },
        },
      });

      if (!teacher) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      await db.transaction(async (dbTrx) => {
        // find existing class
        let schoolClass = await classes.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!schoolClass) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // update class
        await schoolClass.update(req.body);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Class:update" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        attributes: {
          include: [
            [Sequelize.col("user.name"), "teacher_name"],
            [Sequelize.col("class_major.name"), "major_name"],
          ],
        },
        where: {
          period_id: req.query.period_id,
        },
        include: [
          {
            model: class_majors,
            attributes: [],
          },
          {
            model: student_classes,
            required: false,
            separate: true,
            attributes: ["student_id", [Sequelize.col("user.name"), "name"], [Sequelize.col("user.username"), "username"]],
            include: {
              model: users,
              attributes: [],
            },
          },
          {
            model: users,
            attributes: [],
          },
        ],
        order: [
          ["period_id", "desc"],
          ["major_id", "asc"],
          ["grade", "asc"],
          ["name", "asc"],
        ],
      };

      if (req.query.search) {
        query.where = {
          ...query.where,
          [Op.or]: {
            name: {
              [Op.like]: "%" + req.query.search + "%",
            },
            grade: {
              [Op.like]: "%" + req.query.search + "%",
            },
            "$class_major.name$": {
              [Op.like]: "%" + req.query.search + "%",
            },
          },
        };

      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await classes.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "Class:list" });
    }
  }

  static async findOne(req, res, next) {
    try {
      let classroom = await classes.findOne({
        where: {
          id: req.params.id
        },
        attributes: {
          include: [
            [Sequelize.col("user.name"), "teacher_name"],
            [Sequelize.col("class_major.name"), "major_name"],
          ],
        },
        include: [{
          model: class_majors,
          attributes: [],
        }, {
          model: users,
          attributes: [],
        }, {
          model: student_classes,
          required: false,
          separate: true,
          attributes: ["student_id", [Sequelize.col("user.name"), "name"], [Sequelize.col("user.username"), "username"]],
          include: {
            model: users,
            attributes: [],
          },
        }],
      });

      if (!classroom) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(classroom, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "Class:findOne" });
    }
  }

  static async updateStudent(req, res, next) {
    try {
      const schema = joi.object().keys({
        student_ids: joi.array().items(joi.number()).required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate student
      let students = await users.findAll({
        where: {
          id: req.body.student_ids,
        },
        attributes: ["name"],
        include: {
          model: user_roles,
          required: true,
          where: {
            code: "student",
          },
        },
      });

      if (students.length != req.body.student_ids.length) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      // map user input
      let studentClasses = req.body.student_ids.map((item) => {
        return {
          student_id: item,
          class_id: req.params.id,
        };
      });

      await db.transaction(async (dbTrx) => {
        // destroy current student class
        await student_classes.destroy({
          where: {
            class_id: req.params.id,
          },
        });

        // create new student class
        await student_classes.bulkCreate(studentClasses);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Class:updateStudent" });
    }
  }

  static async destroy(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing classroom
        let classroom = await classes.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!classroom) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete classroom
        await classroom.destroy();

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Class:destroy" });
    }
  }

}

module.exports = ClassController;
