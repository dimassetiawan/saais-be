import joi from "joi";
import { Sequelize, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  class_majors,
  class_schedules,
  classes,
  db,
  subjects,
  users,
} from "../models";

class ClassScheduleController {
  static async findOne(req, res, next) {
    try {
      let schedule = await class_schedules.findOne({
        where: {
          id: req.params.id
        },
        include: {
          model: subjects,
          attributes: {
            include: [
              [Sequelize.literal("`subject->class->class_major`.name"), "class_major_name"],
              [Sequelize.literal("`subject->class->class_major`.id"), "class_major_id"],
              [Sequelize.literal("`subject->class`.grade"), "class_grade"],
              [Sequelize.literal("`subject->class`.name"), "class_name"],
            ],
          },
          include: [
            {
              model: users,
              attributes: ["id", "username", "name"],
            },
            {
              model: classes,
              include: [
                {
                  model: class_majors,
                },
              ],
            },
          ],
        },
      });

      if (!schedule) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(schedule, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "ClassSchedule:findOne" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        subQuery: false,
        include: {
          model: subjects,
          attributes: {
            include: [
              [Sequelize.literal("`subject->class->class_major`.name"), "class_major_name"],
              [Sequelize.literal("`subject->class->class_major`.id"), "class_major_id"],
              [Sequelize.literal("`subject->class`.grade"), "class_grade"],
              [Sequelize.literal("`subject->class`.name"), "class_name"],
            ]
          },
          include: [
            {
              model: users,
              attributes: ["id", "username", "name"],
            },
            {
              model: classes,
              attributes: [],
              where: {
                period_id: req.query.period_id,
              },
              include: [
                {
                  model: class_majors,
                  attributes: [],
                },
              ],
            },
          ],
        },
        order: [
          ["day", "asc"],
          ["start_time", "asc"],
        ],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await class_schedules.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "ClassSchedule:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        subject_id: joi.number().required(),
        day: joi.number().min(0).max(6).required(),
        start_time: joi
          .string()
          .regex(/^([0-9]{2})\:([0-9]{2})$/)
          .required(),
        finish_time: joi
          .string()
          .regex(/^([0-9]{2})\:([0-9]{2})$/)
          .required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // create schedule
        await class_schedules.create(req.body);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ClassSchedule:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = joi.object().keys({
        day: joi.number().min(0).max(6).required(),
        start_time: joi
          .string()
          .regex(/^([0-9]{2})\:([0-9]{2})$/)
          .required(),
        finish_time: joi
          .string()
          .regex(/^([0-9]{2})\:([0-9]{2})$/)
          .required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // validate schedule
        let schedule = await class_schedules.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!schedule) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // update schedule
        await schedule.update(req.body);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ClassSchedule:update" });
    }
  }

    static async destroy(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing schedule
        let schedule = await class_schedules.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!schedule) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete schedule
        await schedule.destroy();

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Schedule:destroy" });
    }
  }

}

module.exports = ClassScheduleController;
