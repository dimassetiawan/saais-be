import ResMsg from "../assets/string/ResponseMessage.json";
import {Sequelize, Op, QueryTypes} from "sequelize";
import Response from "../helpers/Response";
import moment from "moment";
import {class_schedules, db, school_attendances, homeroom_teacher_note_results, homeroom_teacher_notes, nonacademic_subject_scores, nonacademic_subjects, student_classes, subjects, classes, competencies, competency_scores, attitudes, school_periods, attitude_scores, attitude_score_details, users, user_roles, user_details, achievements} from "../models";

class ReportController {
  static async studentDashboard(req, res, next) {
    try {
      let attendance = await school_attendances.findOne({
        where: {
          user_id: req.auth.id,
          period_id: req.setting.period_id,
          clock_in_date: moment().format("YYYY-MM-DD"),
        },
      });

      let user = await users.findOne({
        where:{
          id: req.auth.id
        },
        attributes: {
          exclude: ["password", "created_at", "updated_at"],
        },
        include:{
          model: user_details,
          required: false,
          attributes: {
            include: [
              [Sequelize.literal(`coalesce((select amount from school_bills where student_id = users.id and period_id = ${req.setting.period_id} limit 1), 0)`), "bill_amount"],
              [Sequelize.literal(`coalesce((select count(*) from school_bills where student_id = users.id and status = 'due'), 0)`), "bill_due_total"]
            ]
          }
        }
      })

      let competencyScore = await db.query(`select avg(score) total, c.type type from competency_scores cs join competencies c on cs.competency_id = c.id where cs.student_id = 3 and c.id in (select c.id from competencies c join subjects s on c.subject_id = s.id join classes cl on s.class_id = cl.id where cl.period_id = 1) group by c.type`, { type: QueryTypes.SELECT });

      let skillScoreAverage = 0
      let knowledgeScoreAverage = 0

      for await (let item of competencyScore){
        if(item.type == "knowledge"){
          knowledgeScoreAverage = item.total;
        }else{
          skillScoreAverage = item.total;
        }
      }

      let schedule = await class_schedules.findAll({
        where: {
          day: moment().day()
        },
        include: {
          model: subjects,
          include: {
            model: classes,
            include: {
              model: student_classes,
              attributes: [],
              where: {
                student_id: req.auth.id
              }
            }
          } 
        }
      });

      return res.json(Response.success({school_attendance: attendance, user, skill_score_avg: skillScoreAverage, knowledge_score_avg: knowledgeScoreAverage, class_schedules: schedule}, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "Report:studentDashboard" });
    }
  }

  static async getAttitudeScore(userId, periodId){
      let attitudeScoreResult = {};
      let attitudeScores = await attitude_scores.findAll({
        where: {
          student_id: userId,
          period_id: periodId,
        },
        include:{
          model: attitude_score_details,
          include: {
            model: attitudes
          }
        }
      });

      for await (let attitudeScore of attitudeScores){
        let improveStringResult = "";
        let alwaysDoneStringResult = "";
        for await (let item of attitudeScore.attitude_score_details){
          if(item.type == "improve"){
            if(improveStringResult != ""){
              improveStringResult += ", ";
            }
            improveStringResult += item.attitude.description.toLowerCase();
          }else {
            if(alwaysDoneStringResult != ""){
              alwaysDoneStringResult += ", ";
            }
            alwaysDoneStringResult += item.attitude.description.toLowerCase();
          }
        }

        if(alwaysDoneStringResult != ""){
          alwaysDoneStringResult = "Selalu " + alwaysDoneStringResult; 
        }

        if(improveStringResult != ""){
          if(alwaysDoneStringResult != ""){
            improveStringResult = ", sedangkan " + improveStringResult;
          }
          improveStringResult += " mulai berkembang";
        }

        let grade = "";

        switch (attitudeScore.grade){
          case 'SB':
            grade = "Sangat Baik";
            break;
          case 'B':
            grade = "Baik";
            break;
          case 'B':
            grade = "Cukup";
            break;
          default:
            grade = "Kurang";
            break;
        }

        attitudeScoreResult[attitudeScore.type] = {
          grade: grade,
          description: alwaysDoneStringResult + improveStringResult
        }
      }

      return attitudeScoreResult;
  }

  static async getSubjectScore(userId, periodId){
      let subjectScoreResult = {
        knowledge: [],
        skill: [],
        grade: {}
      };

      let subjectScores = await subjects.findAll({
        include:[
        {
          model: classes,
          required: true,
          where:{
            period_id: periodId
          },
          include: {
            model: student_classes,
            where: {
              student_id: userId
            }
          }
        },
        {
          model: competencies,
          include: {
            model: competency_scores,
            where: {
              student_id: userId
            }
          }
        }]
      });

      for await(let subject of subjectScores){
        let scores = {
          knowledge: {
            max : 0,
            min : 0,
            avg : 0,
            maxDesc: "",
            minDesc: "",
            maxGrade: "",
            minGrade: "",
            grade: "",
          },
          skill: {
            max : 0,
            min : 0,
            avg : 0,
            maxDesc: "",
            minDesc: "",
            maxGrade: "",
            minGrade: "",
            grade: "",
          }
        }

        let totalScore = {
          knowledge : [],
          skill : [], 
        }

        for await(let competency of subject.competencies){
          let currentScore = competency.competency_scores[0]?.score ?? 0;

          if(competency.code != "PTS" || competency.code != "PAS"){
            if (currentScore > scores[competency.type].max) {
              scores[competency.type].max = currentScore;
              scores[competency.type].maxDesc = competency.description;
            }

            if (currentScore < scores[competency.type].min || scores[competency.type].min == 0) {
              scores[competency.type].min = currentScore;
              scores[competency.type].minDesc = competency.description;
            }
          }
          
          totalScore[competency.type].push(currentScore);
        }

        scores.knowledge.avg = totalScore.knowledge.length <= 0 ? 0 : Math.round(totalScore.knowledge.reduce((prev, next) => prev + next, 0) / totalScore.knowledge.length);
        scores.skill.avg = totalScore.skill.length <= 0 ? 0 : Math.round(totalScore.skill.reduce((prev, next) => prev + next, 0) / totalScore.skill.length);

        let grade = {
          a: {
            min: 0,
            max: 0,
            desc: "sangat baik"
          },
          b: {
            min: 0,
            max: 0,
            desc: "baik"
          },
          c: {
            min: 0,
            max: 0,
            desc: "cukup"
          },
          d: {
            min: 0,
            max: 0,
            desc: "mulai meningkat"
          },
        };

        grade.c.min = subject.pass_score;
        grade.c.max = Math.floor(grade.c.min + ((100 - grade.c.min )/ 3));
        grade.d.min = 0;
        grade.d.max = grade.c.min == 0 ? 0 : grade.c.min - 1;
        grade.b.min = grade.c.max + 1;
        grade.b.max = Math.floor(grade.b.min + ((100 - grade.b.min )/ 2));
        grade.a.min = grade.b.max + 1;
        grade.a.max = 100;

        for (let key in grade){
          if(scores.knowledge.avg >= grade[key].min && scores.knowledge.avg <= grade[key].max){
            scores.knowledge.grade = key;
          }

          if(scores.knowledge.max >= grade[key].min && scores.knowledge.max <= grade[key].max){
            scores.knowledge.maxGrade = key;
          }

          if(scores.knowledge.min >= grade[key].min && scores.knowledge.min <= grade[key].max){
            scores.knowledge.minGrade = key;
          }

          if(scores.skill.avg >= grade[key].min && scores.skill.avg <= grade[key].max){
            scores.skill.grade = key;
          }

          if(scores.skill.max >= grade[key].min && scores.skill.max <= grade[key].max){
            scores.skill.maxGrade = key;
          }

          if(scores.skill.min >= grade[key].min && scores.skill.min <= grade[key].max){
            scores.skill.minGrade = key;
          }

          if(scores.skill.grade && scores.maxGrade && scores.minGrade && scores.knowledge.grade && scores.knowledge.maxGrade && scores.knowledge.minGrade){
            break;
          }
        }

        let knowledgeDescription = "";
        if(scores.knowledge.maxDesc && scores.knowledge.maxGrade){
          if(["a","b"].includes(scores.knowledge.maxGrade)){
            knowledgeDescription += "Memiliki kemampuan " + grade[scores.knowledge.maxGrade].desc + " dalam " + scores.knowledge.maxDesc; 
          }else if(["c","d"].includes(scores.knowledge.maxGrade)){
            knowledgeDescription += "Memiliki kemampuan " + scores.knowledge.maxDesc + " yang " + grade[scores.knowledge.maxGrade].desc
          }
          
          if(scores.knowledge.maxDesc != scores.knowledge.minDesc && scores.knowledge.minDesc && scores.knowledge.minGrade){
            if(["a","b"].includes(scores.knowledge.minGrade)){
              knowledgeDescription += ", serta kemampuan " + grade[scores.knowledge.minGrade].desc + " dalam " + scores.knowledge.minDesc; 
            }else if(["c","d"].includes(scores.knowledge.minGrade)){
              knowledgeDescription += ", serta kemampuan " + scores.knowledge.minDesc + " yang " + grade[scores.knowledge.minGrade].desc
            }
          }
        }

        let skillDescription = "";
        if(scores.skill.maxDesc && scores.skill.maxGrade){
          if(["a","b"].includes(scores.skill.maxGrade)){
            skillDescription += "Memiliki kemampuan " + grade[scores.skill.maxGrade].desc + " dalam " + scores.skill.maxDesc; 
          }else if(["c","d"].includes(scores.skill.maxGrade)){
            skillDescription += "Memiliki kemampuan " + scores.skill.maxDesc + " yang " + grade[scores.skill.maxGrade].desc
          }
          
          if(scores.skill.maxDesc != scores.skill.minDesc && scores.skill.minDesc && scores.skill.minGrade){
            if(["a","b"].includes(scores.skill.minGrade)){
              skillDescription += ", serta kemampuan " + grade[scores.skill.minGrade].desc + " dalam " + scores.skill.minDesc; 
            }else if(["c","d"].includes(scores.skill.minGrade)){
              skillDescription += ", serta kemampuan " + scores.skill.minDesc + " yang " + grade[scores.skill.minGrade].desc
            }
          }
        }

        subjectScoreResult.knowledge.push({
          name: subject.name,
          pass_score: subject.pass_score,
          score: scores.knowledge.avg,
          grade: scores.knowledge.grade,
          description: knowledgeDescription
        });

        subjectScoreResult.skill.push({
          name: subject.name,
          pass_score: subject.pass_score,
          score: scores.skill.avg,
          grade: scores.skill.grade,
          description: skillDescription
        });

        subjectScoreResult.grade[subject.pass_score] = grade;
      }

      return subjectScoreResult;
  }

  static async getExtracurricularScore(userId, periodId){
    return await nonacademic_subject_scores.findAll({
      where: {
        student_id: userId,
      },
      attributes: [
        "grade",
        [Sequelize.col("nonacademic_subject.name"), "name"]
      ],
      include:{
        model: nonacademic_subjects,
        required: true,
        attributes: [],
        where: {
          period_id: periodId
        }
      }
    });
  }

  static async getAchievement(userId, periodId){
    return await achievements.findAll({
      where: {
        user_id: userId,
        period_id: periodId
      },
      attributes:[[Sequelize.literal(`IF(type = "academic", "Akademik", "Nonakademik")`), 'type'], "description"]
    });
  }

  static async getTeacherNote(userId, periodId){
    let teacherNoteResult = ""
    let teacherNotes = await homeroom_teacher_note_results.findAll({
      where:{
        student_id: userId,
      },
      include:{
        model: homeroom_teacher_notes,
        required: true,
        where:{
          period_id: periodId,
        }
      }
    });

    for await(let item of teacherNotes){
      if(teacherNoteResult != ""){
        teacherNoteResult += ". "
      }

      let currentNote = item.homeroom_teacher_note.description;

      teacherNoteResult += currentNote.charAt(0).toUpperCase() + currentNote.slice(1);

    }

    if(teacherNoteResult != ""){
      teacherNoteResult += "."
    }

    return teacherNoteResult;
  }

    static async getSchoolAttendance(userId, periodId){
    let schoolAttendances = await school_attendances.findAll({
      where: {
        user_id: userId,
        period_id: periodId,
        type : {
          [Op.not]: "h"
        }
      },
      attributes: [
        "type",
        [Sequelize.literal("count(id)"), "total"]
      ],
      group:["school_attendances.type"]
    });

    let attendanceResult = [
      {
        type: "Izin",
        total: 0,
      },
      {
        type: "Sakit",
        total: 0,
      },
      {
        type: "Tanpa Keterangan",
        total: 0,
      }
    ]

    for await(let item of schoolAttendances){
      switch (item.type){
        case "i":
          attendanceResult[0].total = item.dataValues.total ?? 0;
          break;
        case "s":
          attendanceResult[1].total = item.dataValues.total ?? 0;
          break;
        case "a":
          attendanceResult[2].total = item.dataValues.total ?? 0;
          break;
      }
    }

    return attendanceResult;
  }

  static async getHomeroomTeacher(userId, periodId){
    return await users.findOne({
        attributes:[
          "id", "name", "username"
        ],
        include:[{
          model: user_roles,
          attributes: [],
          where: {
            code: "homeroom_teacher"
          }
        },{
          model: classes,
          attributes: [],
          required: true,
          where:{
            period_id : periodId
          },
          include: {
            attributes: [],
            model: student_classes,
            required: true,
            where: {
              student_id: userId
            }
          }
        }]
      });
  }

  static async getPrincipal(){
    return await users.findOne({
        attributes:[
          "id", "name", "username"
        ],
        include:[{
          model: user_roles,
          attributes: [],
          where: {
            code: "principal"
          }
        }]
      });
  }

   static async getPeriod(userId, periodId){
    return await school_periods.findOne({
        where:{
          id: periodId
        },
        attributes:{
          exclude:["created_at", "updated_at", "is_active"]
        }
    });
  }


  static async mainReport(req, res, next){
    try {
      let [attitudeScoreResult,subjectScoreResult,extracurricularScoreResult,achievementResult,teacherNoteResult,attendanceReport,homeroomTeacher, principal, schoolPeriod] = await Promise.all([ReportController.getAttitudeScore(req.params.userId, req.setting.period_id), ReportController.getSubjectScore(req.params.userId, req.setting.period_id), ReportController.getExtracurricularScore(req.params.userId, req.setting.period_id), ReportController.getAchievement(req.params.userId, req.setting.period_id), ReportController.getTeacherNote(req.params.userId, req.setting.period_id), ReportController.getSchoolAttendance(req.params.userId, req.setting.period_id), ReportController.getHomeroomTeacher(req.params.userId, req.setting.period_id), ReportController.getPrincipal(req.params.userId, req.setting.period_id), ReportController.getPeriod(req.params.userId, req.setting.period_id)]  );

      
      return res.json(Response.success({attitude_score: attitudeScoreResult, subject_score: subjectScoreResult, extracurricular_score: extracurricularScoreResult, achievement: achievementResult, teacher_note: teacherNoteResult, attendance_report: attendanceReport, homeroom_teacher: homeroomTeacher, principal, school_period: schoolPeriod}, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "Report:mainReport" });
    }
  }

}

module.exports = ReportController;
