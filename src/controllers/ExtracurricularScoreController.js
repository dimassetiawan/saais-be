import joi from "joi";
import { UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  db,
  nonacademic_subject_scores,
  nonacademic_subjects,
  user_roles,
  users
} from "../models";

class ExtracurricularScoreController {

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }


      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        include: [
          {
            model: users,
            attributes: ["id", "name", "username"]
          },
          {
            required: true,
            model: nonacademic_subjects,
            where: {
              period_id: req.query.period_id
            },
            attributes: []
          }

        ],
        order: [["id", "desc"]],
      };

      if (req.query.nonacademic_subject_id > 0) {
        query.where = {
          ...query.where,
          nonacademic_subject_id: req.query.nonacademic_subject_id
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      let { count, rows } = await nonacademic_subject_scores.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "ExtracurricularScore:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        scores: joi.array().items(
          joi.object().keys({
            student_id: joi.number().required(),
            grade: joi.valid('SB', 'B', 'C', 'K').required(),
          })

        )
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;
      // validate nonacademic subject
      let extracurricular = await nonacademic_subjects.findOne({
        where: {
          id: req.params.exculId,
          period_id: req.body.period_id
        }
      });

      if (!extracurricular) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      let studentIds = [];
      let scoreData = [];

      for await (let item of req.body.scores) {
        studentIds.push(item.student_id);
        scoreData.push({
          nonacademic_subject_id: req.params.exculId,
          ...item
        });
      }

      // validate student
      let students = await users.findAll({
        where: {
          id: studentIds,
        },
        attributes: ["name"],
        include: {
          model: user_roles,
          required: true,
          where: {
            code: "student",
          },
        },
      });

      if (students.length != req.body.scores.length) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }




      await db.transaction(async (dbTrx) => {
        // delete current score
        await nonacademic_subject_scores.destroy({
          where: {
            nonacademic_subject_id: req.params.exculId
          }
        });
        // create score
        await nonacademic_subject_scores.bulkCreate(scoreData);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ExtracurricularScore:create" });
    }
  }

  static async destroy(req, res, next) {
    try {

      let score = await nonacademic_subject_scores.findOne({
        where: {
          id: req.params.id,
        }
      });

      if (!score) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }


      await db.transaction(async (dbTrx) => {
        // create score
        await nonacademic_subject_scores.destroy({
          where: {
            id: req.params.id
          }
        });

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ExtracurricularScore:destroy" });
    }
  }


}

module.exports = ExtracurricularScoreController;
