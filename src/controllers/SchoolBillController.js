import joi from "joi";
import { Sequelize, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  classes,
  db,
  school_bill_payments,
  school_bills,
  school_periods,
  student_classes,
  user_roles,
  users
} from "../models";

class SchoolBillController {
  static async changeBillStatus(
    billId,
    periodId,
    studentIds,
    amount,
    totalPayment
  ) {
    let billQuery = {
      attributes: [
        "id",
        "amount",
        "status",
        [
          Sequelize.literal("sum(school_bill_payments.amount)"),
          "total_payment",
        ],
      ],
      include: {
        model: school_bill_payments,
        attributes: [],
      },
      group: ["school_bills.id"],
      lock: true,
    };

    if (billId) {
      billQuery.where = {
        ...billQuery.where,
        id: billId,
      };
    }

    if (periodId && studentIds) {
      billQuery.where = {
        ...billQuery.where,
        student_id: studentIds,
        period_id: periodId,
      };
    }

    if (!billQuery.where || billQuery.where == {}) {
      throw new Error("Update bill status failed");
    }

    let bills = await school_bills.findAll(billQuery);

    for await (let bill of bills) {
      if (!amount) {
        amount = Number(bill.amount);
      }

      if (!totalPayment) {
        totalPayment = Number(bill.dataValues.total_payment);
      }

      let status = "due";
      if (amount - totalPayment > 0 && totalPayment != 0) {
        status = "partial";
      } else if (amount - totalPayment <= 0) {
        status = "paid";
      }

      await school_bills.update(
        {
          status,
        },
        { where: { id: bill.id } }
      );
    }

    return;
  }

  static async generate(req, res, next) {
    try {
      const schema = joi.object().keys({
        amount: joi.number().required(),
        class_grade: joi.string().valid("x", "xi", "xii").required(),
        force_regenerate: joi.boolean().required(),
        student_ids: joi.array().items(joi.number().optional()).optional(),
        note: joi.string().optional().allow(""),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;

      await db.transaction(async (dbTrx) => {
        // generate bill data
        let studentsQuery = {
          include: [
            {
              model: user_roles,
              required: true,
              attributes: [],
              where: {
                code: "student",
              },
            },
            {
              model: student_classes,
              required: true,
              attributes: [],
              include: {
                model: classes,
                required: true,
                attributes: [],
                where: {
                  period_id: req.body.period_id,
                  grade: req.body.class_grade,
                },
              },
            },
          ],
        };

        if (req.query.student_ids?.length > 0) {
          studentsQuery.where = {
            ...studentsQuery.where,
            id: req.query.student_ids,
          };
        }

        let [students, period] = await Promise.all([users.findAll(studentsQuery), school_periods.findOne({ where: { id: req.body.period_id } })]);

        let months = period.semester == 1 ? [7, 8, 9, 10, 11, 12] : [1, 2, 3, 4, 5, 6];

        let studentIds = await students.map((item) => {
          return item.id;
        });

        let billData = [];

        for await (let month of months) {
          for await (let student of students) {
            billData.push({
              ...req.body,
              student_id: student.id,
              month,
            });
          }
        }

        if (req.body.force_regenerate) {
          // destroy genereated bills
          await school_bills.destroy({
            where: {
              student_id: studentIds,
              period_id: req.body.period_id,
            },
          });
        }

        // generate school bills
        await school_bills.bulkCreate(billData);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolBill:generate" });
    }
  }
  static async findOne(req, res, next) {
    try {
      let bill = await school_bills.findOne({
        where: {
          id: req.params.id
        },
        attributes: {
          include: [[Sequelize.literal('(select coalesce(sum(amount),0) from school_bill_payments where school_bill_id = school_bills.id)'), "paid_amount"]],
        },
        include: [
          {
            model: school_periods,
            attributes: {
              exclude: [
                "created_at", "updated_at"
              ]
            },
          },
          {
            model: school_bill_payments,
            required: false,
            separate: true,
            attributes: {
              include: [[Sequelize.col("user.name"), "operator_name"]],
            },
            include: {
              model: users,
              attributes: [],
            },
          },
        ],
      });

      if (!bill) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(bill, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "SchoolBill:findOne" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        where: {
          period_id: req.query.period_id,
        },
        attributes: {
          include: [[Sequelize.literal('(select coalesce(sum(amount),0) from school_bill_payments where school_bill_id = school_bills.id)'), "paid_amount"]],
        },
        include: [
          {
            model: school_periods,
            attributes: {
              exclude: [
                "created_at", "updated_at"
              ]
            },
          },
          {
            model: school_bill_payments,
            required: false,
            separate: true,
            attributes: {
              include: [[Sequelize.col("user.name"), "operator_name"]],
            },
            include: {
              model: users,
              attributes: [],
            },
          },
        ],
      };

      if (req.query.student_id > 0) {
        query.where = {
          ...query.where,
          student_id: req.query.student_id,
        };
      }

      if (req.query.month > 0) {
        query.where = {
          ...query.where,
          month: req.query.month,
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await school_bills.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "SchoolBill:list" });
    }
  }

  static async createPayment(req, res, next) {
    try {
      const schema = joi.object().keys({
        amount: joi.number().required(),
        payment_type: joi.string().valid("cash", "bank_transfer").required(),
        payment_proof: joi.string().optional().allow(""),
        paid_at: joi.string().isoDate().optional().allow(""),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // remove empty field
      for (let key in req.body) {
        if (!req.body[key]) {
          delete req.body[key];
        }
      }

      await db.transaction(async (dbTrx) => {
        // find existing bill
        let bill = await school_bills.findOne({
          where: {
            id: req.params.id,
          },
          attributes: {
            include: [
              [
                Sequelize.literal("sum(school_bill_payments.amount)"),
                "total_payment",
              ],
            ],
          },
          include: {
            model: school_bill_payments,
            attributes: [],
          },
          lock: true,
        });

        if (!bill) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        bill.amount = Number(bill.amount);
        let totalPayment =
          Number(bill.dataValues.total_payment) + req.body.amount;

        // validate total payment so not exceeding bill
        if (bill.amount - totalPayment < 0) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }

        // create payment
        await school_bill_payments.create({
          ...req.body,
          school_bill_id: bill.id,
          created_by: req.auth.id,
        });

        // update bill
        await SchoolBillController.changeBillStatus(
          bill.id,
          null,
          null,
          null,
          totalPayment
        );

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolBill:createPayment" });
    }
  }

  static async deletePayment(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing bill
        let payment = await school_bill_payments.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!payment) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete payment
        await payment.destroy();

        // update bill
        await SchoolBillController.changeBillStatus(
          payment.school_bill_id,
          null,
          null,
          null,
          null
        );

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolBill:deletePayment" });
    }
  }
}

module.exports = SchoolBillController;
