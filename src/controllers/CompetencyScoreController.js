import joi from "joi";
import { Sequelize, Op, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  classes,
  competencies,
  competency_scores,
  db,
  student_classes,
  subjects,
  users,
  user_roles
} from "../models";

class CompetencyScoreController {
  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        competency_id: joi.number().required(),
        scores: joi.array().items(
          joi.object().keys({
            student_id: joi.number().required(),
            score: joi.number().max(100).required(),
          })
        )
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate student and competency
      let competency = await competencies.findOne({
        where: {
          id: req.body.competency_id,
        },
        attributes: ["id"],
        include: {
          model: subjects,
          attributes: ["id"],
          required: true,
          include: {
            model: classes,
            required: true,
            attributes: ["id"],
            where: {
              period_id: req.setting.period_id,
            } ,
          },
        },
      });

      if (!competency) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      let studentIds = [];
      let scoreData = [];

      for await (let item of req.body.scores) {
        studentIds.push(item.student_id);
        scoreData.push({
          competency_id: req.body.competency_id,
          ...item
        });
      }

      // validate student
      let students = await users.findAll({
        where: {
          id: studentIds,
        },
        attributes: ["name"],
        include: [{
          model: user_roles,
          required: true,
          attributes:[],
          where: {
            code: "student",
          },
        },
        {
          model: student_classes,
          required: true,
          attributes: [],
          where: {
            class_id: competency.subject.class.id,
          }
        }],
      });

      if (students.length != req.body.scores.length) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      await db.transaction(async (dbTrx) => {
        // create score
        await competency_scores.bulkCreate(scoreData, {
          updateOnDuplicate: ["score"]
        });

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "CompetencyScore:create" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        subQuery: false,
        include: [
         {
            model: users,
            attributes: ["id", "name", "username"]
          },
          {
          model: competencies,
          required: true,
          attributes: [],
          include: {
            model: subjects,
            attributes: [],
            required: true,
            include: {
              model: classes,
              required: true,
              attributes: [],
              where: {
                period_id: req.query.period_id,
              },
            },
          },
        }],
        order: [[users, "name", "asc"]],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      if(req.query.competency_id > 0){
        query.where = {
          ...query.where,
          competency_id : req.query.competency_id
        }
      }

      const { count, rows } = await competency_scores.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "CompetencyScore:list" });
    }
  }
}

module.exports = CompetencyScoreController;
