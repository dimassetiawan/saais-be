import Response from '../helpers/Response';

class TestController {
  static async test(req, res, next) {
    try {
      return res.json(Response.success('Sukses'));
    } catch (error) {
      next({ error, fun: 'Test:test' });
    }
  }

}

module.exports = TestController;
