import joi from "joi";
import { UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  db,
  homeroom_teacher_note_results,
  homeroom_teacher_notes,
  user_roles,
  users
} from "../models";

class HomeroomTeacherNoteResultController {


  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        include: {
          model: homeroom_teacher_notes,
          required: true,
          where: {
            period_id: req.query.period_id
          }
        }
      };

      if (req.query.student_id > 0) {
        query.where = {
          ...query.where,
          student_id: req.query.student_id
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      let { count, rows } = await homeroom_teacher_note_results.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "HomeroomTeacherNoteResult:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        student_id: joi.number().required(),
        note_ids: joi.array().items(joi.number().required()).required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;
      let student = await users.findOne({
        where: {
          id: req.body.student_id,
        },
        attributes: ["name"],
        include: {
          model: user_roles,
          required: true,
          where: {
            code: "student",
          },
        },
      });

      if (!student) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      let noteResultData = [];
      for await (let item of req.body.note_ids) {
        noteResultData.push({
          student_id: req.body.student_id,
          period_id: req.setting.period_id,
          note_id: item
        });
      }

      await db.transaction(async (dbTrx) => {
        // delete current note
        await homeroom_teacher_note_results.destroy({
          where: {
            student_id: req.body.student_id,
            period_id: req.setting.period_id,
          },
        });
        // create note
        await homeroom_teacher_note_results.bulkCreate(noteResultData);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "HomeroomTeacherNoteResult:create" });
    }
  }

}

module.exports = HomeroomTeacherNoteResultController;
