import bcrypt from "bcrypt";
import crypto from "crypto";
import joi from "joi";
import _ from "lodash";
import { Op, Sequelize, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import RedisClient from "../helpers/Redis";
import { getUserData } from "../helpers/UserUtil";
import {
  cities,
  class_majors,
  classes,
  competencies,
  competency_scores,
  db,
  roles,
  school_periods,
  student_classes,
  subjects,
  user_details,
  user_roles,
  users
} from "../models";

class UserController {
  static async findOne(req, res, next) {
    try {
      // get user data
      let user = await getUserData(null, req.params.id);

      delete user.password;

      if (!user) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      user.student_classes = await student_classes.findAll({
        where:{
          student_id: user.id
        },
        attributes : [
          [Sequelize.literal("`class->class_major`.name"), "class_major_name"],
          [Sequelize.literal("`class`.name"), "class_name"],
          [Sequelize.literal("`class`.grade"), "class_grade"],
        ],
        include: {
          model: classes,
          attributes : [],
          required: true,
          where: {
            period_id: req.setting.period_id,
          },
          include: [
            {
              attributes : [],
              model: class_majors,
            },
            {
              model: school_periods,
              attributes: {
                exclude: ["created_at", "updated_at"]
              }
            },
          ],
        },
      })

      return res.json(Response.success(user, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "User:profile" });
    }
  }

  static async getCurrentUser(req, res, next) {
    try {
      // get user data
      let user = await getUserData(null, req.auth.id);

      delete user.password;

      if (!user) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      user.student_classes = await student_classes.findAll({
        where:{
          student_id: user.id
        },
        attributes : [
          [Sequelize.literal("`class->class_major`.name"), "class_major_name"],
          [Sequelize.literal("`class`.name"), "class_name"],
          [Sequelize.literal("`class`.grade"), "class_grade"],
        ],
        include: {
          model: classes,
          attributes : [],
          required: true,
          where: {
            period_id: req.setting.period_id,
          },
          include: [
            {
              attributes : [],
              model: class_majors,
            },
            {
              model: school_periods,
              attributes: {
                exclude: ["created_at", "updated_at"]
              }
            },
          ],
        },
      })

      return res.json(Response.success(user, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "User:profile" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        username: joi.string().min(4).max(100).trim().required(),
        name: joi.string().min(4).max(100).trim().required(),
        user_roles: joi
          .array()
          .items(
            joi
              .object()
              .keys({
                code: joi.string().required(),
              })
              .required()
          )
          .required(),
        user_detail: joi
          .object()
          .keys({
            nisn: joi.number().optional().allow(""),
            nik: joi.number().optional().allow(""),
            address: joi.string().trim().optional().allow(""),
            place_of_birth_id: joi.number().optional().allow(""),
            date_of_birth: joi.string().isoDate().optional().allow(""),
            gender: joi.string().valid("male", "female").optional().allow(""),
            religion: joi.string().max(100).trim().optional().allow(""),
            education: joi.string().max(100).trim().optional().allow(""),
            phone_num: joi.string().max(100).trim().optional().allow(""),
            status_in_family: joi.string().max(100).trim().optional().allow(""),
            order_in_family: joi.number().optional().allow(""),
            origin_school: joi.string().max(100).trim().optional().allow(""),
            join_date: joi.string().isoDate().optional().allow(""),
            join_class: joi
              .string()
              .valid("x", "xi", "xii")
              .optional()
              .allow(""),
            father_name: joi.string().max(100).trim().optional().allow(""),
            father_occupation: joi
              .string()
              .max(100)
              .trim()
              .optional()
              .allow(""),
            mother_name: joi.string().max(100).trim().optional().allow(""),
            mother_occupation: joi
              .string()
              .max(100)
              .trim()
              .optional()
              .allow(""),
            parent_phone_num: joi.string().max(100).trim().optional().allow(""),
            parent_address: joi.string().trim().optional().allow(""),
            alumni_education: joi.string().max(100).trim().optional().allow(""),
            alumni_job: joi.string().max(100).trim().optional().allow(""),
            graduated_at: joi.string().isoDate().optional().allow(""),
          })
          .optional(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // remove empty field
        for (let key in req.body) {
          if (!req.body[key]) {
            delete req.body[key];
          }
        }

        // create and encrypt password
        const salt = await bcrypt.genSalt(12);
        req.body.password = await bcrypt.hash(
          crypto.randomBytes(32).toString("hex"),
          salt
        );

        // create user
        let user = await users.create(req.body);

        // let historyData = [
        //   {
        //     action: "User:create",
        //     table_name: "users",
        //     table_id: user.id,
        //     created_by: req.auth.id,
        //     previous_data: user.toJSON(),
        //   },
        // ];

        // remove old user roles
        await user_roles.destroy({
          where: {
            user_id: user.id,
          },
          lock: true,
        });

        for await (let role of req.body.user_roles) {
          role.user_id = user.id;
        }

        // create new user roles
        await user_roles.bulkCreate(req.body.user_roles);

        // historyData.push({
        //   action: "User:create",
        //   table_name: "user_roles.user_id",
        //   table_id: user.id,
        //   created_by: req.auth.id,
        //   previous_data: req.body.roles,
        // });

        // create or update user detail if exist
        if (req.body.user_detail && req.body.user_detail != {}) {
          // delete empty field
          for (let key in req.body.user_detail) {
            if (!req.body.user_detail[key]) {
              delete req.body.user_detail[key];
            }
          }

          let userDetails = await user_details.create({
            ...req.body.user_detail,
            user_id: user.id,
          });

          // historyData.push({
          //   action: "User:create",
          //   table_name: "user_details.user_id",
          //   table_id: user.id,
          //   created_by: req.auth.id,
          //   previous_data: userDetails.toJSON(),
          // });
        }

        // await createHistory(historyData);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.user.duplicateUsername.code)
          .json(Response.error(ResMsg.user.duplicateUsername.message));
      }
      next({ error, fun: "User:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = joi.object().keys({
        username: joi.string().min(4).max(100).trim().required(),
        name: joi.string().min(4).max(100).trim().required(),
        password: joi.string().min(6).max(100).trim().optional().allow(""),
        old_password: joi.string().min(6).max(100).trim().optional().allow(""),
        is_active: joi.boolean().optional().allow(""),
        is_reset: joi.boolean().optional().allow(""),
        user_roles: joi
          .array()
          .items(
            joi
              .object()
              .keys({
                code: joi.string().required(),
              })
              .optional()
          )
          .optional(),
        user_detail: joi
          .object()
          .keys({
            nisn: joi.number().optional().allow(""),
            nik: joi.number().optional().allow(""),
            address: joi.string().trim().optional().allow(""),
            place_of_birth_id: joi.number().optional().allow(""),
            date_of_birth: joi.string().isoDate().optional().allow(""),
            gender: joi.string().valid("male", "female").optional().allow(""),
            religion: joi.string().max(100).trim().optional().allow(""),
            education: joi.string().max(100).trim().optional().allow(""),
            phone_num: joi.string().max(100).trim().optional().allow(""),
            status_in_family: joi.string().max(100).trim().optional().allow(""),
            order_in_family: joi.number().optional().allow(""),
            origin_school: joi.string().max(100).trim().optional().allow(""),
            join_date: joi.string().isoDate().optional().allow(""),
            join_class: joi
              .string()
              .valid("x", "xi", "xii")
              .optional()
              .allow(""),
            father_name: joi.string().max(100).trim().optional().allow(""),
            father_occupation: joi
              .string()
              .max(100)
              .trim()
              .optional()
              .allow(""),
            mother_name: joi.string().max(100).trim().optional().allow(""),
            mother_occupation: joi
              .string()
              .max(100)
              .trim()
              .optional()
              .allow(""),
            parent_phone_num: joi.string().max(100).trim().optional().allow(""),
            parent_address: joi.string().trim().optional().allow(""),
            alumni_education: joi.string().max(100).trim().optional().allow(""),
            alumni_job: joi.string().max(100).trim().optional().allow(""),
            graduated_at: joi.string().isoDate().optional().allow(""),
          })
          .optional(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // find user in db
        let user = await users.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!user) {
          return res
            .status(ResMsg.user.notFound.code)
            .json(Response.error(ResMsg.user.notFound.message));
        }

        // remove empty field
        for (let key in req.body) {
          if (!req.body[key]) {
            delete req.body[key];
          }
        }

        // create and encrypt password
        if (req.body.password) {
          let isValid = await bcrypt.compare(
            req.body.old_password ?? "",
            user.password
          );

          if (!isValid) {
            return res
              .status(ResMsg.auth.invalidPassword.code)
              .json(Response.error(ResMsg.auth.invalidPassword.message));
          }

          const salt = await bcrypt.genSalt(12);
          req.body.password = await bcrypt.hash(req.body.password, salt);
        }

        let prevData = user.toJSON();
        delete prevData.password;

        // update user
        await user.update(req.body);

        // let historyData = [
        //   {
        //     action: "User:update",
        //     table_name: "users",
        //     table_id: user.id,
        //     created_by: req.auth.id,
        //     previous_data: prevData,
        //   },
        // ];

        // delete redis data if user not active
        if (!user.is_active) {
          await RedisClient.del("SaaisUserData:" + user.id);
        }

        // create or update user roles if exist
        if (req.body.user_roles && req.body.user_roles.length > 0) {
          // remove old user roles
          await user_roles.destroy({
            where: {
              user_id: user.id,
            },
            lock: true,
          });

          for await (let role of req.body.user_roles) {
            role.user_id = user.id;
          }

          // create new user roles
          await user_roles.bulkCreate(req.body.user_roles);

          // historyData.push({
          //   action: "User:update",
          //   table_name: "user_roles.user_id",
          //   table_id: user.id,
          //   created_by: req.auth.id,
          //   previous_data: req.body.user_roles,
          // });
        }

        // create or update user detail if exist
        if (req.body.user_detail && req.body.user_detail != {}) {
          // delete empty field
          for (let key in req.body.user_detail) {
            if (!req.body.user_detail[key]) {
              delete req.body.user_detail[key];
            }
          }

          let userDetails = await user_details.findOne({
            where: {
              user_id: user.id,
            },
          });
          user;
          let prevData;
          if (userDetails) {
            await userDetails.update(req.body.user_detail);
          } else {
            await user_details.create({
              ...req.body.user_detail,
              user_id: user.id,
            });
          }

          // historyData.push({
          //   action: "User:update",
          //   table_name: "user_details.user_id",
          //   table_id: user.id,
          //   created_by: req.auth.id,
          //   previous_data: prevData,
          // });
        }

        // await createHistory(historyData);
      });

      await getUserData(null, req.params.id, true);

      return res.json(Response.success());
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.user.duplicateUsername.code)
          .json(Response.error(ResMsg.user.duplicateUsername.message));
      }
      next({ error, fun: "User:update" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      let roleFilter = {};
      if (req.query.role) {
        roleFilter = {
          code: req.query.role.split(",")
        };
      }

      let classFilter = {};
      if (req.query.class_id > 0) {
        classFilter = {
          class_id: req.query.class_id
        };
      }


      let billQuery = [];

      if (req.query.includeBill) {
        billQuery = [
          [Sequelize.literal(`coalesce((select amount from school_bills where student_id = users.id and period_id = ${req.setting.period_id} limit 1), 0)`), "bill_amount"],
          [Sequelize.literal(`coalesce((select count(*) from school_bills where student_id = users.id and status = 'due'), 0)`), "bill_due_total"]
        ];
      }

      let attendanceQuery = [];

      if (req.query.includeAttendance) {
        attendanceQuery = [
          [Sequelize.literal(`coalesce((select count(*) from school_attendances where user_id = users.id and type = 'h' and period_id = ${req.setting.period_id}), 0)`), "attendance_h_total"],
          [Sequelize.literal(`coalesce((select count(*) from school_attendances where user_id = users.id and type = 'a' and period_id = ${req.setting.period_id}), 0)`), "attendance_a_total"],
          [Sequelize.literal(`coalesce((select count(*) from school_attendances where user_id = users.id and type = 'i' and period_id = ${req.setting.period_id}), 0)`), "attendance_i_total"],
          [Sequelize.literal(`coalesce((select count(*) from school_attendances where user_id = users.id and type = 's' and period_id = ${req.setting.period_id}), 0)`), "attendance_s_total"],
        ];
      }

      let achievementQuery = [];

      if (req.query.includeAchievement) {
        achievementQuery = [
          [Sequelize.literal(`coalesce((select count(*) from achievements where user_id = users.id and period_id = ${req.setting.period_id}), 0)`),
            "achievement_total"]
        ];
      }

      let attitudeQuery = [];

      if (req.query.includeAttitude) {
        attitudeQuery = [
          [Sequelize.literal(`coalesce((select grade from attitude_scores where type = "spiritual" and student_id = users.id and period_id = ${req.setting.period_id}), "-")`),
            "attitude_spiritual"],
          [Sequelize.literal(`coalesce((select grade from attitude_scores where type = "sosial" and student_id = users.id and period_id = ${req.setting.period_id}), "-")`),
            "attitude_sosial"]
        ];
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        subQuery: false,
        attributes: {
          exclude: ["password", "created_at", "updated_at"],
        },
        include: [
          {
            model: user_details,
            required: false,
            attributes: {
              exclude: ["created_at", "updated_at"],
              include: [
                [Sequelize.literal("`user_detail->city`.name"), "birth_city_name"],
                [
                  Sequelize.literal("`user_detail->city`.province_name"),
                  "birth_city_province_name",
                ],
                ...billQuery,
                ...attendanceQuery,
                ...achievementQuery,
                ...attitudeQuery
              ],
            },
            include: {
              model: cities,
              required: false,
              attributes: [],
            },
          },
          {
            model: user_roles,
            required: _.isEmpty(roleFilter) ? false : true,
            attributes: ["code", [Sequelize.literal("`user_roles->role`.name"), "name"],],
            include: {
              model: roles,
              attributes: [],
            },
            where: roleFilter,
          },
          {
            model: student_classes,
            required: _.isEmpty(classFilter) ? false : true,
            where: classFilter,
            attributes : [
                [Sequelize.literal("`student_classes->class->class_major`.name"), "class_major_name"],
                [Sequelize.literal("`student_classes->class`.name"), "class_name"],
                [Sequelize.literal("`student_classes->class`.grade"), "class_grade"],
              ],
            include: {
              model: classes,
              attributes : [],
              required: true,
              where: {
                period_id: req.setting.period_id,
              },
              include: [
                {
                  attributes : [],
                  model: class_majors,
                },
                {
                  model: school_periods,
                  attributes: {
                    exclude: ["created_at", "updated_at"]
                  }
                },
              ],
            },
          },
        ],
        order: [["name", "asc"]],
      };

      let countQuery = {}

      if (req.query.includeScore) {
        countQuery = {...query};

        let scoreFilter = {}
        if(req.query.subject_id > 0){
          scoreFilter = {
            subject_id: req.query.subject_id
          }
        }

        query.include = [
          ...query.include,
        {
          model: competency_scores,
          required: false,
          include: {
            model: competencies,
            required: true,
            where: scoreFilter
          }
        }]
      }

      // filter for teacher
      if (req.query.subject_id > 0 && !req.query.includeScore) {
        query.include = [
          ...query.include,
          {
            model: subjects,
            required: true,
            where: {
              id: req.query.subject_id
            }
          }
        ];
      }

      if (req.query.search) {
        query.where = {
          ...query.where,
          [Op.or]: {
            username: {
              [Op.like]: "%" + req.query.search + "%",
            },
            name: {
              [Op.like]: "%" + req.query.search + "%",
            },
          },
        };

        query.include[0].where = {
          ...query.include[0].where,
          [Op.or]: {
            nik: {
              [Op.like]: "%" + req.query.search + "%",
            },
            nisn: {
              [Op.like]: "%" + req.query.search + "%",
            },
          }
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      let { count, rows } = await users.findAndCountAll(query);

      if(req.query.includeScore){
        count = await users.count(countQuery);
      }

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "User:list" });
    }
  }

  static async destroy(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing user
        let user = await users.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!user) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete user
        await user.destroy();

        await RedisClient.del("SaaisUserData:" + req.params.id);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "User:destroy" });
    }
  }

  static async resetPass(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing user
        let user = await users.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!user) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete user
        await user.update({
          is_reset: true
        });

        let resetCode = Math.random().toString(36).slice(2, 7).toUpperCase();

        await RedisClient.set(
          "SaaisResetCode:" + user.username,
          resetCode
        );
        await RedisClient.expire("SaaisResetCode:" + user.username, 60 * 2);

        return res.json(Response.success({code: resetCode}));
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "User:destroy" });
    }
  }

}

module.exports = UserController;
