import { Op } from "sequelize";
import { createHistory } from "../helpers/History";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import { db, histories } from "../models";

class HistoryController {
  static async del(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // clear history
        await histories.destroy({
          where: {},
          truncate: true,
        });

        // create history
        await createHistory([
          {
            action: "History:del",
            table_name: "histories",
            created_by: req.auth.id,
          },
        ]);

        return res.json(Response.success());
      });
    } catch (error) {
      next({ error, fun: "History:del" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);
      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        order: [["created_at", "desc"]],
      };

      if (req.query.search) {
        query.where = {
          ...query.where,
          action: {
            [Op.like]: "%" + req.query.search + "%",
          },
        };
      }

      if (req.query.startDate && req.query.endDate) {
        query.where = {
          ...query.where,
          createdAt: {
            [Op.between]: [req.query.startDate, req.query.endDate],
          },
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await histories.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "History:list" });
    }
  }
}

module.exports = HistoryController;
