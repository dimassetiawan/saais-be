import joi from "joi";
import { Op, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  achievements,
  db,
  nonacademic_subject_scores,
  user_roles,
  users
} from "../models";

class AchievementController {

  static async findOne(req, res, next) {
    try {
      let extracurricular = await achievements.findOne({
        where: {
          id: req.params.id
        },
        include: [
          {
            model: users,
            attributes: ["id", "name"]
          },
          {
            model: nonacademic_subject_scores
          }
        ],
      });

      if (!extracurricular) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(extracurricular, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "Achievement:findOne" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }


      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        include: [
          {
            model: users,
            required: true,
            attributes: ["id", "name"]
          },
        ],
        where: {
          period_id: req.query.period_id
        },
        order: [["id", "desc"]],
      };

      if (req.query.user_id > 0) {
        query.where = {
          ...query.where,
          user_id: req.query.user_id
        };
      }

      if (['academic', 'nonacademic'].includes(req.query.type)) {
        query.where = {
          ...query.where,
          type: req.query.type
        };
      }

      if (req.query.search) {
        query.where = {
          ...query.where,
          name: {
            [Op.like]: "%" + req.query.search + "%"
          }
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      let { count, rows } = await achievements.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "Achievement:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        user_id: joi.number().required(),
        type: joi.string().valid('academic', 'nonacademic').required(),
        description: joi.string().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;
      // validate coach
      if (req.body.user_id) {
        let student = await users.findOne({
          where: {
            id: req.body.user_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "student",
            },
          },
        });

        if (!student) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // create achievement
        await achievements.create(req.body);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Achievement:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = joi.object().keys({
        user_id: joi.number().required(),
        type: joi.string().valid('academic', 'nonacademic').required(),
        description: joi.string().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate student
      if (req.body.student) {
        let student = await users.findOne({
          where: {
            id: req.body.user_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "student",
            },
          },
        });

        if (!student) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        // update achievements
        await achievements.update(
          req.body,
          {
            where: {
              id: req.params.id,
            },
          }
        );


        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Achievement:update" });
    }
  }

  static async destroy(req, res, next) {
    try {

      let achievement = await achievements.findOne({
        where: {
          id: req.params.id
        }
      });

      if (!achievement) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }


      await db.transaction(async (dbTrx) => {
        // remove achievements
        await achievement.destroy();


        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Achievement:destroy" });
    }
  }


}

module.exports = AchievementController;
