import joi from "joi";
import { Op, Sequelize, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  class_majors,
  classes,
  competencies,
  db,
  subjects,
  user_roles,
  users
} from "../models";

class SubjectController {

  static async findOne(req, res, next) {
    try {
      let subject = await subjects.findOne({
        where: {
          id: req.params.id
        },
        attributes: {
          include: [
            [Sequelize.literal("`class->class_major`.name"), "class_major_name"],
            [Sequelize.literal("`class->class_major`.id"), "class_major_id"],
            [Sequelize.literal("`class`.name"), "class_name"],
            [Sequelize.literal("class.grade"), "class_grade"],
          ],
        },
        include: [
          {
            model: competencies
          },
          {
            model: users,
            attributes: ["id", "name"]
          },
          {
            model: classes,
            required: true,
            attributes: [],
            include: {
              model: class_majors,
              attributes: [],
            },
          },
        ],
      });

      if (!subject) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(subject, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "Subject:findOne" });
    }
  }


  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        subQuery: false,
        attributes: {
          include: [
            [Sequelize.literal("`class->class_major`.name"), "class_major_name"],
            [Sequelize.literal("`class->class_major`.id"), "class_major_id"],
            [Sequelize.literal("`class`.name"), "class_name"],
            [Sequelize.literal("class.grade"), "class_grade"],
          ],
        },
        include: [
          {
            model: competencies
          },
          {
            model: users,
            attributes: ["id", "name"]
          },
          {
            model: classes,
            required: true,
            attributes: [],
            where: {
              period_id: req.query.period_id,
            },
            include: {
              model: class_majors,
              attributes: [],
            },
          },
        ],
        order: [["name", "asc"]],
      };

      // if class_id is empty the results are grouped
      if (req.query.class_id) {
        query.attributes.include = [
          ...query.attributes.include,
          [Sequelize.literal("user.name"), "teacher_name"],
        ];
        query.include = [
          ...query.include,
          {
            model: users,
            attributes: [],
          },
        ];
        query.where = {
          ...query.where,
          class_id: req.query.class_id,
        };
      } else {
        query.group = [
          "class.grade",
          "class.major_id",
          "subjects.code",
          "subjects.name",
        ];
        query.attributes.exclude = [
          "id",
          "teacher_id",
          "class_id",
          "pass_score",
        ];
      }

      if (req.query.search) {
        query.where = {
          ...query.where,
          name: {
            [Op.like]: "%" + req.query.search + "%",
          }
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      let { count, rows } = await subjects.findAndCountAll(query);

      if (req.query.class_id < 0) {
        count = count.length;
      }

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "Subject:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        name: joi.string().min(4).max(100).required(),
        code: joi.string().max(10).required(),
        class_grade: joi.string().valid("x", "xi", "xii").required(),
        class_major_id: joi.number().required(),
        class_ids: joi.array().items(joi.number().optional()).optional(),
        teacher_id: joi.number().optional().allow(""),
        force_regenerate: joi.bool().optional(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // remove empty field
      for (let key in req.body) {
        if (!req.body[key]) {
          delete req.body[key];
        }
      }

      // validate teacher
      if (req.body.teacher_id) {
        let teacher = await users.findOne({
          where: {
            id: req.body.teacher_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "teacher",
            },
          },
        });

        if (!teacher) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }
      }

      let classQuery = {
        where: {
          major_id: req.body.class_major_id,
          grade: req.body.class_grade,
          period_id: req.setting.period_id,
        },
      };

      if (req.body.class_id?.length > 0) {
        classQuery.where = {
          ...classQuery.where,
          id: req.body.class_ids,
        };
      }

      // search class with the criteria
      let currentClasses = await classes.findAll(classQuery);

      if (currentClasses.length == 0) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      let subjectData = [];
      let classIds = []

      for await (let item of currentClasses) {
        classIds.push(item.id);

        subjectData.push({
          class_id: item.id,
          ...req.body,
        });
      }

      await db.transaction(async (dbTrx) => {
        if (req.body.force_generate) {
          await subjects.destroy({
            where: {
              class_id: classIds,
              code: req.body.code,
              name: req.body.name
            }
          });
        }
        // create subject
        await subjects.bulkCreate(subjectData, {
          updateOnDuplicate: ['teacher_id'],
        });

        let createdSubjects = await subjects.findAll({
          where:{
            class_id: classIds,
            code: req.body.code,
            name: req.body.name
          }
        })

        let competencyData = [];
        for await (let item of createdSubjects){
          competencyData.push({
            subject_id: item.id,
            code: "PTS",
            type: "knowledge",
            description: "Penilaian Tengah Semester"
          });

          competencyData.push({
            subject_id: item.id,
            code: "PAS",
            type: "knowledge",
            description: "Penilaian Akhir Semester"
          });
        }

        await competencies.bulkCreate(competencyData, {
          updateOnDuplicate: ["description"]
        });

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Subject:create" });
    }
  }

  // static async update(req, res, next) {
  //   try {
  //     const schema = joi.object().keys({
  //       pass_score: joi.number().min(0).max(100).optional().allow(),
  //       teacher_id: joi.number().optional().allow(),
  //       subject_code: joi.string().required(),
  //       class_grade: joi.string().valid("x", "xi", "xii").required(),
  //       class_major_id: joi.number().required(),
  //       class_ids: joi.array().items(joi.number().optional()).optional(),
  //       grades: joi
  //         .array()
  //         .items(
  //           joi
  //             .object()
  //             .keys({
  //               code: joi.string().max(10).required(),
  //               description: joi.string().max(100).required(),
  //               min_score: joi.number().min(0).max(100).required(),
  //             })
  //             .required()
  //         )
  //         .optional(),
  //     });

  //     const validate = schema.validate(req.body);

  //     if (validate.error) {
  //       if (process.env.NODE_ENV == "production") {
  //         return res
  //           .status(ResMsg.general.invalidInput.code)
  //           .json(Response.error(ResMsg.general.invalidInput.message));
  //       } else {
  //         return res
  //           .status(ResMsg.general.invalidInput.code)
  //           .json(Response.error(validate.error.message));
  //       }
  //     }

  //     // validate teacher
  //     if (req.body.teacher_id) {
  //       let teacher = await users.findOne({
  //         where: {
  //           id: req.body.teacher_id,
  //         },
  //         attributes: ["name"],
  //         include: {
  //           model: user_roles,
  //           required: true,
  //           where: {
  //             code: "teacher",
  //           },
  //         },
  //       });

  //       if (!teacher) {
  //         return res
  //           .status(ResMsg.general.invalidInput.code)
  //           .json(Response.error(ResMsg.general.invalidInput.message));
  //       }
  //     }

  //     await db.transaction(async (dbTrx) => {
  //       let subjectQuery = {
  //         where: {
  //           code: req.body.subject_code,
  //         },
  //         include: {
  //           model: classes,
  //           attributes: [],
  //           required: true,
  //           where: {
  //             major_id: req.body.class_major_id,
  //             grade: req.body.class_grade,
  //             period_id: req.setting.period_id,
  //           },
  //         },
  //         lock: true,
  //       };

  //       if (req.body.class_id?.length > 0) {
  //         subjectQuery.where = {
  //           ...subjectQuery.where,
  //           id: req.body.class_ids,
  //         };
  //       }

  //       // search subject with the criteria
  //       let currentSubjects = await subjects.findAll(subjectQuery);

  //       if (currentSubjects.length <= 0) {
  //         return res
  //           .status(ResMsg.general.notFound.code)
  //           .json(Response.error(ResMsg.general.notFound.message));
  //       }

  //       let subjectIds = [];
  //       let gradeData = [];

  //       for await (let item of currentSubjects) {
  //         subjectIds.push(item.id);
  //         for await (let grade of req.body.grades) {
  //           gradeData.push({
  //             subject_id: item.id,
  //             ...grade,
  //           });
  //         }
  //       }

  //       // update subjects
  //       await subjects.update(
  //         {
  //           pass_score: req.body.pass_score,
  //         },
  //         {
  //           where: {
  //             id: subjectIds,
  //           },
  //         }
  //       );

  //       // update grade
  //       if (req.body.grades?.length > 0) {
  //         // remove old grades
  //         await grades.destroy({
  //           where: {
  //             subject_id: subjectIds,
  //           },
  //         });

  //         // create new grades
  //         await grades.bulkCreate(gradeData);
  //       }

  //       return res.json(Response.success());
  //     });
  //   } catch (error) {
  //     if (error instanceof UniqueConstraintError) {
  //       return res
  //         .status(ResMsg.general.duplicateRecord.code)
  //         .json(Response.error(ResMsg.general.duplicateRecord.message));
  //     }
  //     next({ error, fun: "Subject:update" });
  //   }
  // }

  static async update(req, res, next) {
    try {
      const schema = joi.object().keys({
        pass_score: joi.number().min(0).max(100).optional().allow(),
        teacher_id: joi.number().optional().allow(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate teacher
      if (req.body.teacher_id) {
        let teacher = await users.findOne({
          where: {
            id: req.body.teacher_id,
          },
          attributes: ["name"],
          include: {
            model: user_roles,
            required: true,
            where: {
              code: "teacher",
            },
          },
        });

        if (!teacher) {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        }
      }

      await db.transaction(async (dbTrx) => {
        let subject = await subjects.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!subject) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        await subject.update(req.body);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Subject:update" });
    }
  }

  static async destroy(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing subject
        let subject = await subjects.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!subject) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete subject
        await subject.destroy();

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Subject:destroy" });
    }
  }


}

module.exports = SubjectController;
