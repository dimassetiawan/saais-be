import joi from "joi";
import { Op, Sequelize, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  class_attendances,
  class_majors,
  class_sessions,
  classes,
  db,
  student_classes,
  subjects,
  user_roles,
  users,
} from "../models";

class ClassSessionController {
  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      let subjectFilter = {};
      if (req.query.class_id > 0) {
        subjectFilter = {
          class_id: req.query.class_id
        };
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        subQuery: false,
        include: [
          {
            model: class_attendances,
            required: false,
            include: {
              model: users,
              attributes: ["id", "username", "name"]
            }
          },
          {
            model: users,
            attributes: ["id", "name"]
          },
          {
            model: subjects,
            required: true,
            where:
              subjectFilter,
            attributes: {
              include: [
                [Sequelize.literal("`subject->class->class_major`.name"), "class_major_name"],
                [Sequelize.literal("`subject->class->class_major`.id"), "class_major_id"],
                [Sequelize.literal("`subject->class`.grade"), "class_grade"],
                [Sequelize.literal("`subject->user`.name"), "teacher_name"],
              ],
            },
            include: [
              {
                model: users,
                attributes: ["id", "name"]
              },
              {
                model: classes,
                where: {
                  period_id: req.query.period_id,
                },
                include: [
                  {
                    model: class_majors,
                    attributes: []
                  },

                ],
              },
            ],
          },
        ],
        order: [["started_at", "desc"]],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await class_sessions.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "ClassSession:list" });
    }
  }

  static async findOne(req, res, next) {
    try {
      let session = await class_sessions.findOne({
        where: {
          id: req.params.id
        },
        include: [
          {
            model: class_attendances,
            required: false,
            include: {
              model: users,
              attributes: ["id", "name", "username"]
            }
          },
          {
            model: users,
            attributes: ["id", "name"]
          },
          {
            model: subjects,
            required: true,
            attributes: {
              include: [
                [Sequelize.literal("`subject->class->class_major`.name"), "class_major_name"],
                [Sequelize.literal("`subject->class->class_major`.id"), "class_major_id"],
                [Sequelize.literal("`subject->class`.grade"), "class_grade"],
                [Sequelize.literal("`subject->user`.name"), "teacher_name"],
              ],
            },
            include: [
              {
                model: users,
                attributes: ["id", "name"]
              },
              {
                model: classes,
                include: [
                  {
                    model: class_majors,
                    attributes: []
                  },

                ],
              },
            ],
          },
        ],
      });

      if (!session) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(session, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "ClassSession:findOne" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        teacher_id: joi.number().optional(),
        subject_id: joi.number().required(),
        lesson_note: joi.string().required(),
        started_at: joi.string().isoDate().required(),
        finished_at: joi.string().isoDate().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      if (!req.body.teacher_id) {
        req.body.teacher_id = req.auth.id;
      }

      let teacher = await users.findOne({
        where: {
          id: req.body.teacher_id,
        },
        attributes: ["name"],
        include: {
          model: user_roles,
          required: true,
          where: {
            code: "teacher",
          },
        },
      });

      if (!teacher) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      await db.transaction(async (dbTrx) => {
        // create session
        let session = await class_sessions.create(req.body);


        let students = await users.findAll({
        attributes: ["id"],
        include: [
          {
            model: user_roles,
            required: true,
            attributes: [],
            where: {
              code: "student",
            },
          },
          {
            model: student_classes,
            attributes: [],
            required: true,
            include: {
              model: classes,
              attributes: [],
              required: true,
              include: {
                model: subjects,
                attributes: [],
                required: true,
                where: {
                  id: req.body.subject_id
                },
              },
            },
          },
        ],
        });

        let attendanceData = [];

        for await (let student of students) {
          attendanceData.push({
            class_session_id: session.id,
            student_id: student.id,
            type: "h",
          });
        }

        await class_attendances.bulkCreate(attendanceData);


        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ClassSession:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = joi.object().keys({
        teacher_id: joi.number().optional(),
        lesson_note: joi.string().optional().allow(),
        started_at: joi.string().isoDate().required(),
        finished_at: joi.string().isoDate().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      if (!req.body.teacher_id) {
        req.body.teacher_id = req.auth.id;
      }

      let teacher = await users.findOne({
        where: {
          id: req.body.teacher_id,
        },
        attributes: ["name"],
        include: {
          model: user_roles,
          required: true,
          where: {
            code: "teacher",
          },
        },
      });

      if (!teacher) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      await db.transaction(async (dbTrx) => {
        let session = await class_sessions.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!session) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // update session
        await session.update(req.body);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ClassSession:update" });
    }
  }

  static async createAttendanceByClass(req, res, next) {
    try {
      const schema = joi.object().keys({
        absent_students: joi
          .array()
          .items(
            joi.object().keys({
              student_id: joi.number().required(),
              type: joi.string().valid("a", "i", "s").required(),
              reason: joi.string().optional().allow(""),
            })
          )
          .required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      let attendanceData = [];

      let absentStudentIds = await req.body.absent_students.map((item) => {
        // generate absent student data
        attendanceData.push({
          ...item,
          class_session_id: req.params.id,
        });

        return item.student_id;
      });

      // validate absent student
      let absentStudents = await users.findAll({
        where: {
          id: absentStudentIds,
        },
        include: [
          {
            model: user_roles,
            required: true,
            attributes: [],
            where: {
              code: "student",
            },
          },
          {
            model: student_classes,
            attributes: [],
            required: true,
            include: {
              model: classes,
              attributes: [],
              required: true,
              include: {
                model: subjects,
                attributes: [],
                required: true,
                include: {
                  model: class_sessions,
                  attributes: [],
                  required: true,
                  where: {
                    id: req.params.id,
                  },
                },
              },
            },
          },
        ],
      });

      if (absentStudents.length != req.body.absent_students.length) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      // generate attend student data
      let students = await users.findAll({
        where: {
          id: {
            [Op.notIn]: absentStudentIds,
          },
        },
        attributes: ["id"],
        include: [
          {
            model: user_roles,
            required: true,
            attributes: [],
            where: {
              code: "student",
            },
          },
          {
            model: student_classes,
            attributes: [],
            required: true,
            include: {
              model: classes,
              attributes: [],
              required: true,
              include: {
                model: subjects,
                attributes: [],
                required: true,
                include: {
                  model: class_sessions,
                  attributes: [],
                  required: true,
                  where: {
                    id: req.params.id,
                  },
                },
              },
            },
          },
        ],
      });

      for await (let student of students) {
        attendanceData.push({
          class_session_id: req.params.id,
          student_id: student.id,
          type: "h",
        });
      }

      await db.transaction(async (dbTrx) => {
        // bulk create attendance
        await class_attendances.bulkCreate(attendanceData);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ClassSession:createAttendanceByClass" });
    }
  }

  static async createAttendance(req, res, next) {
    try {
      const schema = joi.object().keys({
        student_ids: joi.array().items(joi.number()).required(),
      });


      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      let session = await class_sessions.findOne({
        where: {
          id: req.params.id,
        },
        attributes: ["id"],
        include: {
          model: subjects,
          attributes: ["class_id"]
        }
      });

      if (!session) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      let students = await users.findAll({
        where: {
          id: req.body.student_ids,
        },
        attributes: ["id", "name"],
        include: [{
          model: user_roles,
          required: true,
          where: {
            code: "student",
          },
        }, {
          model: student_classes,
          required: true,
          where: {
            class_id: session.subject.class_id
          }
        }]
      });

      if (students.length != req.body.student_ids.length) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      let attendaces = [];
      for await (let item of students) {
        attendaces.push({
          class_session_id: req.params.id,
          student_id: item.id,
          type: "h"
        });
      }

      await db.transaction(async (dbTrx) => {
        // destroy old attendances
        await class_attendances.destroy({
          where: {
            class_session_id: req.params.id,
          },
        });

        await class_attendances.bulkCreate(attendaces);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ClassSession:createAttendance" });
    }
  }

  static async destroy(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing session
        let session = await class_sessions.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!session) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete session
        await session.destroy();

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "ClassSession:destroy" });
    }
  }

}

module.exports = ClassSessionController;
