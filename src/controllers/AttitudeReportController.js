import joi from "joi";
import { Sequelize, Op, UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  classes,
  attitude_scores,
  attitudes,
  attitude_score_details,
  competencies,
  competency_scores,
  db,
  student_classes,
  subjects,
  users,
  user_roles
} from "../models";

class AttitudeReportController {
  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        student_id: joi.number().required(),
        sosial_grade: joi.string().valid('SB','B','C','K'),
        always_done_sosial_attitudes: joi.array().items(
          joi.number().optional()
        ).required(),
        improve_sosial_attitudes: joi.array().items(
          joi.number().optional()
        ).required(),
        spiritual_grade: joi.string().valid('SB','B','C','K'),
        always_done_spiritual_attitudes: joi.array().items(
          joi.number().optional()
        ).required(),
        improve_spiritual_attitudes: joi.array().items(
          joi.number().optional()
        ).required(),
      });


      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      // validate student
      let user = await  users.findOne({
        where: {
          id: req.body.student_id,
        },
        attributes: ["name"],
        include: [{
          model: user_roles,
          required: true,
          attributes:[],
          where: {
            code: "student",
          },
        }]})

      if (!user) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      // validate attitude
      let spiritualAttitudes = await attitudes.findAll({
        where: {
          id: [...req.body.always_done_spiritual_attitudes, ...req.body.improve_spiritual_attitudes],
          period_id: req.setting.period_id,
          type: 'spiritual'
        },
        attributes: ["id"],
      });

      if (spiritualAttitudes.length != (req.body.always_done_spiritual_attitudes.length + req.body.improve_spiritual_attitudes.length)) {
        return res
          .status(ResMsg.general.invalidInput.code)
          .json(Response.error(ResMsg.general.invalidInput.message));
      }

      await db.transaction(async (dbTrx) => {

        let [currentSpiritualAttitude, currentSosialAttitude] = await Promise.all([
          attitude_scores.findOne({
            where:{
              period_id: req.setting.period_id,
            student_id: req.body.student_id,
            type: "spiritual",
            }
          }),
          attitude_scores.findOne({
            where:{
              period_id: req.setting.period_id,
            student_id: req.body.student_id,
            type: "sosial",
            }
          }),
        ]) 

        let [spiritualAttitude] = await attitude_scores.upsert({
          id: currentSpiritualAttitude?.id,
          period_id: req.setting.period_id,
          student_id: req.body.student_id,
          type: "spiritual",
          grade: req.body.spiritual_grade
        });

        let [sosialAttitude] = await attitude_scores.upsert({
          id: currentSosialAttitude?.id,
          period_id: req.setting.period_id,
          student_id: req.body.student_id,
          type: "sosial",
          grade: req.body.sosial_grade
        });

        let alwaysDoneSpiritualScores = [];

        for await (let item of req.body.always_done_spiritual_attitudes) {
          alwaysDoneSpiritualScores.push({
            attitude_score_id: spiritualAttitude.id,
            attitude_id: item,
            type: 'always_done'
          });
        }

        let improveSpiritualScores = [];

        for await (let item of req.body.improve_spiritual_attitudes) {
          improveSpiritualScores.push({
            attitude_score_id: spiritualAttitude.id,
            attitude_id: item,
            type: 'improve'
          });
        }

        let alwaysDoneSosialScores = [];

        for await (let item of req.body.always_done_sosial_attitudes) {
          alwaysDoneSosialScores.push({
            attitude_score_id: sosialAttitude.id,
            attitude_id: item,
            type: 'always_done'
          });
        }

        let improveSosialScores = [];

        for await (let item of req.body.improve_sosial_attitudes) {
          improveSosialScores.push({
            attitude_score_id: sosialAttitude.id,
            attitude_id: item,
            type: 'improve'
          });
        }

        await attitude_score_details.destroy({
          where:{
            attitude_score_id: [sosialAttitude.id, spiritualAttitude.id]
          }
        })

        
        await attitude_score_details.bulkCreate([
          ...alwaysDoneSpiritualScores, 
          ...improveSpiritualScores, 
          ...alwaysDoneSosialScores, 
          ...improveSosialScores
          ], 
        );

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "AttitudeReport:create" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        subQuery: false,
        where:{
          period_id: req.query.period_id,
        },
        include: [{
          model: attitude_score_details,
          include:{
            model: attitudes
          }
        }],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      if(req.query.student_id > 0){
        query.where = {
          ...query.where,
          student_id: req.query.student_id
        }
      }

      const { count, rows } = await attitude_scores.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "AttitudeReport:list" });
    }
  }
}

module.exports = AttitudeReportController;
