import bcrypt from "bcrypt";
import joi from "joi";
import ResMsg from "../assets/string/ResponseMessage.json";
import RedisClient from "../helpers/Redis";
import Response from "../helpers/Response";
import { getToken, getUserData } from "../helpers/UserUtil";
import {users} from "../models"

class AuthController {
  static async login(req, res, next) {
    try {
      const schema = joi.object().keys({
        username: joi.string().min(4).max(100).trim().required(),
        password: joi.string().min(6).max(100).trim().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      //get user data
      let user = await getUserData(req.body.username, null, true);

      if (user && user.is_active) {
        // validate for reset password
        if (user.is_reset) {
          return res
            .status(ResMsg.auth.resetPasswordRequired.code)
            .json(Response.error(ResMsg.auth.resetPasswordRequired.message));
        }

        // validate password
        let isValid = await bcrypt.compare(req.body.password, user.password);

        delete user.password;

        if (isValid) {
          // get token
          let token = await getToken(user);

          return res.json(Response.success({ token, user }));
        } else {
          return res
            .status(ResMsg.auth.invalidCredential.code)
            .json(Response.error(ResMsg.auth.invalidCredential.message));
        }
      } else {
        return res
          .status(ResMsg.auth.invalidCredential.code)
          .json(Response.error(ResMsg.auth.invalidCredential.message));
      }
    } catch (error) {
      next({ error, fun: "Auth:login" });
    }
  }

   static async resetPass(req, res, next) {
    try {
      const schema = joi.object().keys({
        username: joi.string().min(4).max(100).trim().required(),
        password: joi.string().min(6).max(100).trim().required(),
        code: joi.string().required()
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      //get user data
      let user = await getUserData(req.body.username, null, true);

      if (user && user.is_active) {
        // validate for reset password
        if (!user.is_reset) {
           return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
        }

        let code = await RedisClient.get("SaaisResetCode:" + req.body.username);

        if (code && (req.body.code == code)) {
          const salt = await bcrypt.genSalt(12);
          let password = await bcrypt.hash(
            req.body.password,
            salt
          );

          await users.update({
            password: password,
            is_reset: false,
          },{
            where:{
              id: user.id
            }
          })


          return res.json(Response.success());
        } else {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }
      } else {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }
    } catch (error) {
      next({ error, fun: "Auth:resetPass" });
    }
  }


  // static async selectRole(req, res, next) {
  //   try {
  //     const schema = joi.object().keys({
  //       code: joi.string().min(4).max(100).trim().required(),
  //     });

  //     const validate = schema.validate(req.body);

  //     if (validate.error) {
  //       if (process.env.NODE_ENV == "production") {
  //         return res
  //           .status(ResMsg.general.invalidInput.code)
  //           .json(Response.error(ResMsg.general.invalidInput.message));
  //       } else {
  //         return res
  //           .status(ResMsg.general.invalidInput.code)
  //           .json(Response.error(validate.error.message));
  //       }
  //     }

  //     //get user data
  //     let user = await getUserData(req.auth.username, null, true);

  //     if (user && user.is_active) {
  //       // validate for reset password
  //       if (user.is_reset) {
  //         return res
  //           .status(ResMsg.auth.resetPasswordRequired.code)
  //           .json(Response.error(ResMsg.auth.resetPasswordRequired.message));
  //       }

  //       delete user.password;

  //       // filter role
  //       user.user_roles = user.user_roles.filter(
  //         (item) => item.code == req.body.code
  //       );

  //       if (user.user_roles.length <= 0) {
  //         return res
  //           .status(ResMsg.general.invalidInput.code)
  //           .json(Response.error(ResMsg.general.invalidInput.message));
  //       }

  //       // get token
  //       let token = await getToken(user);

  //       return res.json(Response.success({ token }));
  //     } else {
  //       return res
  //         .status(ResMsg.auth.invalidCredential.code)
  //         .json(Response.error(ResMsg.auth.invalidCredential.message));
  //     }
  //   } catch (error) {
  //     next({ error, fun: "Auth:selectRole" });
  //   }
  // }

  static async logout(req, res, next) {
    try {
      // delete user date in redis
      await RedisClient.del("SaaisUserData:" + req.auth.id);
      return res.json(Response.success());
    } catch (error) {
      next({ error, fun: "Auth:logout" });
    }
  }
}

module.exports = AuthController;
