import joi from "joi";
import { UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  attitudes,
  db
} from "../models";

class AttitudeController {

  static async findOne(req, res, next) {
    try {
      let atttitude = await attitudes.findOne({
        where: {
          id: req.params.id
        },
      });

      if (!atttitude) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(atttitude, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "Attitude:findOne" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,

      };

      if (["spiritual", "sosial"].includes(req.query.type)) {
        query.where = {
          ...query.where,
          type: req.query.type
        };
      }

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      let { count, rows } = await attitudes.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "Attitude:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        type: joi.string().valid('spiritual', 'sosial').required(),
        description: joi.string().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;

      await db.transaction(async (dbTrx) => {
        // create attitude

        await attitudes.create(req.body);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Attitude:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = joi.object().keys({
        type: joi.string().valid('spiritual', 'sosial').required(),
        description: joi.string().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }


      await db.transaction(async (dbTrx) => {
        // update attitudes
        await attitudes.update(
          req.body,
          {
            where: {
              id: req.params.id,
            },
          }
        );


        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Attitude:update" });
    }
  }

  static async destroy(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing attitude
        let attitude = await attitudes.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!attitude) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete attitude
        await attitude.destroy();

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "Attitude:destroy" });
    }
  }

}

module.exports = AttitudeController;
