import moment from "moment";
import { UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import RedisClient from "../helpers/Redis";
import Response from "../helpers/Response";
import { db, school_periods } from "../models";

class SchoolPeriodController {
  static async create(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        let currentPeriod = await school_periods.findOne({
          order: [["created_at", "desc"]],
        });

        let periodData = {};

        if (currentPeriod) {
          // set current period not active
          await school_periods.update(
            {
              is_active: false,
            },
            {
              where: {
                is_active: true,
              },
            }
          );

          if (currentPeriod.semester == 1) {
            periodData = {
              start_year: currentPeriod.start_year,
              semester: 2,
            };
          } else {
            periodData = {
              start_year: Number(currentPeriod.end_year),
              semester: 1,
            };
          }
        } else {
          if (moment().month() < 7) {
            periodData = {
              start_year: moment().subtract(1, "year").format("YYYY"),
              semester: 2,
            };
          } else {
            periodData = {
              start_year: moment().format("YYYY"),
              semester: 1,
            };
          }
        }

        periodData.end_year = Number(periodData.start_year) + 1;

        // create period
        await school_periods.create(periodData);

        // delete period in redis
        await RedisClient.del("SaaisSetting:SchoolPeriod");

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "SchoolPeriod:create" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);
      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
        attributes: {
          exclude: ["created_at", "updated_at"],
        },
        order: [
          ["start_year", "desc"],
          ["semester", "desc"],
        ],
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      const { count, rows } = await school_periods.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "SchoolPeriod:list" });
    }
  }
}

module.exports = SchoolPeriodController;
