import joi from "joi";
import { UniqueConstraintError } from "sequelize";
import ResMsg from "../assets/string/ResponseMessage.json";
import { pagingUtil } from "../helpers/Paging";
import Response from "../helpers/Response";
import {
  db,
  homeroom_teacher_notes
} from "../models";

class HomeroomTeacherNoteController {

  static async findOne(req, res, next) {
    try {
      let note = await homeroom_teacher_notes.findOne({
        where: {
          id: req.params.id
        },
      });

      if (!note) {
        return res
          .status(ResMsg.general.notFound.code)
          .json(Response.error(ResMsg.general.notFound.message));
      }

      return res.json(Response.success(note, ResMsg.general.emptyMessage));
    } catch (error) {
      next({ error, fun: "HomeroomTeacherNote:findOne" });
    }
  }

  static async list(req, res, next) {
    try {
      const paging = pagingUtil(req);

      if (!req.query.period_id) {
        req.query.period_id = req.setting.period_id;
      }

      const query = {
        offset: (paging.page - 1) * paging.perPage,
        limit: paging.perPage,
      };

      if (req.query.perPage == -1) {
        delete query.offset;
        delete query.limit;
      }

      let { count, rows } = await homeroom_teacher_notes.findAndCountAll(query);

      if (req.query.perPage == -1) {
        paging.perPage = count;
      }

      return res.send(Response.indexPaging(count, paging, rows));
    } catch (error) {
      next({ error, fun: "HomeroomTeacherNote:list" });
    }
  }

  static async create(req, res, next) {
    try {
      const schema = joi.object().keys({
        description: joi.string().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }

      req.body.period_id = req.setting.period_id;

      await db.transaction(async (dbTrx) => {
        // create note
        await homeroom_teacher_notes.create(req.body);

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "HomeroomTeacherNote:create" });
    }
  }

  static async update(req, res, next) {
    try {
      const schema = joi.object().keys({
        description: joi.string().required(),
      });

      const validate = schema.validate(req.body);

      if (validate.error) {
        if (process.env.NODE_ENV == "production") {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(ResMsg.general.invalidInput.message));
        } else {
          return res
            .status(ResMsg.general.invalidInput.code)
            .json(Response.error(validate.error.message));
        }
      }


      await db.transaction(async (dbTrx) => {
        // update homeroom_teacher_notes
        await homeroom_teacher_notes.update(
          req.body,
          {
            where: {
              id: req.params.id,
            },
          }
        );


        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "HomeroomTeacherNote:update" });
    }
  }

  static async destroy(req, res, next) {
    try {
      await db.transaction(async (dbTrx) => {
        // find existing teacherNote
        let teacherNote = await homeroom_teacher_notes.findOne({
          where: {
            id: req.params.id,
          },
          lock: true,
        });

        if (!teacherNote) {
          return res
            .status(ResMsg.general.notFound.code)
            .json(Response.error(ResMsg.general.notFound.message));
        }

        // delete teacherNote
        await teacherNote.destroy();

        return res.json(Response.success());
      });
    } catch (error) {
      if (error instanceof UniqueConstraintError) {
        return res
          .status(ResMsg.general.duplicateRecord.code)
          .json(Response.error(ResMsg.general.duplicateRecord.message));
      }
      next({ error, fun: "HomeroomTeacherNote:destroy" });
    }
  }

}

module.exports = HomeroomTeacherNoteController;
