import ResMsg from "../assets/string/ResponseMessage.json";
import Response from "../helpers/Response";

class HomeController {
  static async welcome(req, res, next) {
    try {
      return res.json(Response.success(null, ResMsg.general.welcome.message));
    } catch (error) {
      next({ error, fun: "Home:welcome" });
    }
  }
}

module.exports = HomeController;
