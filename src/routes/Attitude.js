import { Router } from "express";
import {
  create,
  findOne,
  list,
  update,
  destroy
} from "../controllers/AttitudeController";

const routes = Router();

routes.put("/attitude/:id", update);
routes.post("/attitude", create);
routes.get("/attitude/:id", findOne);
routes.get("/attitude", list);
routes.delete("/attitude/:id", destroy);

module.exports = routes;
