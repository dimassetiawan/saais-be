import { Router } from "express";
import {
  clockInOut,
  createByClass,
  destroy,
  list,
  manualCreate,
} from "../controllers/SchoolAttendanceController";

const routes = Router();

routes.post("/school-attendance/by-class", createByClass);
routes.post("/school-attendance", manualCreate);
routes.post("/school-attendance/realtime", clockInOut);
routes.get("/school-attendance", list);
routes.delete("/school-attendance/:id", destroy);

module.exports = routes;
