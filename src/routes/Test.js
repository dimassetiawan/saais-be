import { Router } from "express";
import { test } from "../controllers/TestController";

const routes = Router();

routes.post("/test", test);

module.exports = routes;
