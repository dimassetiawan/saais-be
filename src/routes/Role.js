import { Router } from "express";
import { list } from "../controllers/RoleController";

const routes = Router();

routes.get("/role", list);

module.exports = routes;
