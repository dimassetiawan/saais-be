import { Router } from "express";
import { create, list } from "../controllers/SchoolPeriodController";

const routes = Router();

routes.post("/period", create);
routes.get("/period", list);

module.exports = routes;
