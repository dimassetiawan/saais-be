import { Router } from "express";
import {
  create,
  list
} from "../controllers/HomeroomTeacherNoteResultController";

const routes = Router();

routes.post("/teacher-note-result", create);
routes.get("/teacher-note-result", list);

module.exports = routes;
