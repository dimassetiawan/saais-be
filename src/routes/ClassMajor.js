import { Router } from "express";
import { list } from "../controllers/ClassMajorController";

const routes = Router();

routes.get("/class-major", list);

module.exports = routes;
