import { Router } from "express";
import { create, list } from "../controllers/AttitudeReportController";

const routes = Router();

routes.get("/attitude-report", list);
routes.post("/attitude-report", create);

module.exports = routes;
