import { Router } from "express";
import {
  create,
  findOne,
  list,
  update,
  destroy
} from "../controllers/ExtracurricularController";

const routes = Router();

routes.put("/extracurricular/:id", update);
routes.post("/extracurricular", create);
routes.get("/extracurricular/:id", findOne);
routes.get("/extracurricular", list);
routes.delete("/extracurricular/:id", destroy);

module.exports = routes;
