import { Router } from "express";
import { create, findOne, list, update, destroy } from "../controllers/ClassScheduleController";

const routes = Router();

routes.put("/schedule/:id", update);
routes.post("/schedule", create);
routes.get("/schedule", list);
routes.get("/schedule/:id", findOne);
routes.delete("/schedule/:id", destroy);

module.exports = routes;
