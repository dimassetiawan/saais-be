import { Router } from "express";
import { studentDashboard, mainReport } from "../controllers/ReportController";

const routes = Router();

routes.get("/report/student-dashboard", studentDashboard);
routes.get("/report/main-report/:userId", mainReport);

module.exports = routes;
