import { Router } from 'express';
import { welcome } from '../controllers/HomeController';

const routes = Router();

routes.get('/', welcome);

module.exports = routes;
