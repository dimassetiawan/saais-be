import { Router } from "express";
import { login, logout, resetPass } from "../controllers/AuthController";
import LoginCheck from "../middlewares/LoginCheck";

const routes = Router();

routes.post("/auth/login", login);
routes.post("/auth/reset-pass", resetPass);
// routes.put("/auth/role", LoginCheck, selectRole);
routes.delete("/auth/logout", LoginCheck, logout);

module.exports = routes;
