import { Router } from "express";
import { create, findOne, getCurrentUser, list, update, destroy, resetPass } from "../controllers/UserController";
import RoleValidator from "../middlewares/RoleValidator";

const routes = Router();

routes.get("/user/me", getCurrentUser);
routes.get("/user/:id", findOne);
routes.put("/user/:id/reset-pass", resetPass);
routes.put("/user/:id", (req, res, next) => RoleValidator(req, res, next, ["superadmin"]), update);
routes.post("/user", (req, res, next) => RoleValidator(req, res, next, ["superadmin"]), create);
routes.get("/user", (req, res, next) => RoleValidator(req, res, next, ["superadmin", "teacher", "homeroom_teacher", "finance",]), list);
routes.delete("/user/:id", destroy);

module.exports = routes;
