import { Router } from "express";
import {
  create,
  list
} from "../controllers/ExtracurricularScoreController";

const routes = Router();

routes.post("/extracurricular-score/:exculId", create);
routes.get("/extracurricular-score", list);
routes.delete("/extracurricular-score/:id", list);

module.exports = routes;
