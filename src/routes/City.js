import { Router } from "express";
import { list } from "../controllers/CityController";

const routes = Router();

routes.get("/city", list);

module.exports = routes;
