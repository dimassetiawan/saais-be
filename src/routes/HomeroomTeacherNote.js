import { Router } from "express";
import {
  create,
  findOne,
  list,
  update,
  destroy
} from "../controllers/HomeroomTeacherNoteController";

const routes = Router();

routes.put("/teacher-note/:id", update);
routes.post("/teacher-note", create);
routes.get("/teacher-note/:id", findOne);
routes.get("/teacher-note", list);
routes.delete("/teacher-note/:id", destroy);

module.exports = routes;
