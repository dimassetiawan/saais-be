import { Router } from "express";
import {
  create,
  findOne,
  list,
  update,
  updateStudent,
  destroy
} from "../controllers/ClassController";

const routes = Router();

routes.put("/class/:id", update);
routes.put("/class/:id/student", updateStudent);
routes.post("/class", create);
routes.get("/class/:id", findOne);
routes.get("/class", list);
routes.delete("/class/:id", destroy);

module.exports = routes;
