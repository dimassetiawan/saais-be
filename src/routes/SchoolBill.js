import { Router } from "express";
import {
  createPayment,
  deletePayment,
  findOne,
  generate,
  list
} from "../controllers/SchoolBillController";

const routes = Router();

routes.post("/school-bill", generate);
routes.delete("/school-bill/payment/:id", deletePayment);
routes.put("/school-bill/:id/payment", createPayment);
routes.get("/school-bill", list);
routes.get("/school-bill/:id", findOne);

module.exports = routes;
