import { Router } from "express";
import { create, list } from "../controllers/CompetencyScoreController";

const routes = Router();

routes.get("/competency-score", list);
routes.post("/competency-score", create);

module.exports = routes;
