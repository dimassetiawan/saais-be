import { Router } from "express";
import {
  create,
  destroy,
  findOne,
  list,
  update
} from "../controllers/AchievementController";

const routes = Router();

routes.put("/achievement/:id", update);
routes.post("/achievement", create);
routes.get("/achievement/:id", findOne);
routes.get("/achievement", list);
routes.delete("/achievement/:id", destroy);

module.exports = routes;
