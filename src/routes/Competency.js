import { Router } from "express";
import { create, destroy, list } from "../controllers/CompetencyController";

const routes = Router();

routes.get("/competency", list);
routes.post("/competency", create);
routes.delete("/competency/:id", destroy);

module.exports = routes;
