import { Router } from "express";
import { create, findOne, list, update, destroy } from "../controllers/SubjectController";

const routes = Router();

routes.get("/subject", list);
routes.put("/subject/:id", update);
routes.post("/subject", create);
routes.get("/subject/:id", findOne);
routes.delete("/subject/:id", destroy);

module.exports = routes;
