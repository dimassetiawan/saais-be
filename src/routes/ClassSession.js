import { Router } from "express";
import {
  create,
  createAttendance,
  createAttendanceByClass,
  findOne,
  list,
  update,
  destroy
} from "../controllers/ClassSessionController";

const routes = Router();

routes.put("/session/:id", update);
routes.put("/session/:id/attendance", createAttendance);
routes.put("/session/:id/attendance-by-class", createAttendanceByClass);
routes.post("/session", create);
routes.get("/session", list);
routes.get("/session/:id", findOne);
routes.delete("/session/:id", destroy);

module.exports = routes;
