# Backend Saais - Smaisga Academic Information System

## Description
Backend for Saais - Smaisga Academic Information System (Siakad / Sistem Informasi Akademik). Build using ExpressJS, MySQL, and Redis. Scoring model used in this system is K13 scoring model

# Features
- User, Class, Subject, and Competency management
- Student Score and Report
- School and Class Attendance management
- School Bill and Payment

## Installation
- Install MariaDB/MySQL and redis
- Clone this repo
- Create new database
- Set env and rename .env.example to .env
- Run "npm install"
- Run "npm run migrate"
- Run "npm start"

## API Spefication
Install Insomnia (https://github.com/Kong/insomnia/releases) and import insomnia-spec.json to get API spesification

## Authors and Acknowledgment
Dimas Setiawan (dimasdhimek@gmail.com)

## License
This program is free software.
It is licensed under the GNU GPL version 3 or later.
That means you are free to use this program for any purpose;
free to study and modify this program to suit your needs;
and free to share this program or your modifications with anyone.
If you share this program or your modifications
you must grant the recipients the same freedoms.
To be more specific: you must share the source code under the same license.
For details see https://www.gnu.org/licenses/gpl-3.0.html
